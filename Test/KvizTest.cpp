#include <QDebug>
#include "../catchme/include/igrac.h"
#include "../catchme/include/kviz.h"
#include "catch.hpp"



TEST_CASE("Testiranje konstruktora klase Kviz","[konstruktor]"){
    SECTION("Konstruktor Kviz postoji"){
        //Arrange
        QString ime="Andjela";
        Boja boja=Boja::Crna;
        Igrac* igrac=new Igrac(1,ime,boja);
        QString ime2="Marija";
        Boja boja2=Boja::Roze;
        Igrac* igrac2=new Igrac(2, ime2, boja2);
        QList<Igrac* > igraci={igrac, igrac2};
        //SpisakPitanja* sp=new SpisakPitanja();

        //Act
        Kviz* kviz=new Kviz(4, igraci,0);

        //Assert
        REQUIRE_NOTHROW(kviz);
    }

}


TEST_CASE("Testiranje destruktora klase Kviz","[destruktor]"){
    SECTION("Destruktor ~Kviz postoji"){
        //Arrange
        QString ime="Andjela";
        Boja boja=Boja::Crna;
        Igrac* igrac=new Igrac(1,ime,boja);
        QString ime2="Marija";
        Boja boja2=Boja::Roze;
        Igrac* igrac2=new Igrac(2, ime2, boja2);
        QList<Igrac* > igraci={igrac, igrac2};
        //SpisakPitanja* sp=new SpisakPitanja();

        //Act

        Kviz* kviz=new Kviz(4,igraci,0);
        delete kviz;

        //Assert
        REQUIRE_NOTHROW(kviz);
    }
}


