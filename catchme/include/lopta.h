#ifndef LOPTA_H
#define LOPTA_H

#include <QGraphicsItem>
#include <QTimer>
#include <QPainter>
#include <QBrush>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsEllipseItem>
#include <QStyleOption>
#include <QList>
#include "blok.h"

class Lopta: public QGraphicsRectItem, public QObject
{
public:
    Lopta(QGraphicsRectItem* parent = nullptr);

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
                  const QStyleOptionGraphicsItem *option,
                  QWidget *widget) override;

    //void mousePressEvent(QGraphicsSceneMouseEvent * event) override;
    void sudarSaReketom(QList<QGraphicsItem*> cItems);
    void sudarSaBlokom(QList<QGraphicsItem*> cItems);
    void sudarSaIvicomProzora(double sirinaEkrana, double visinaEkrana);
    double getXkoordCentra();
    QList<QGraphicsItem*> myCollidingItems(QList<QGraphicsRectItem*> items);
    QList<Blok *> myCollidingBlocks(QList<Blok*> items);

    double xBrzina() const;
    void setXBrzina(double newXBrzina);
    double yBrzina() const;
    void setYBrzina(double newYBrzina);
    QColor boja() const;
    void setBoja(const QColor &newBoja);

protected:
    void timer(QTimerEvent *);
    // Za svaki frejm u animaciji,
    // scena poziva advance nad elementima scene.
    void advance(int step) override;


private:
    double _xBrzina;
    double _yBrzina;
    QColor _boja;

};

#endif // LOPTA_H
