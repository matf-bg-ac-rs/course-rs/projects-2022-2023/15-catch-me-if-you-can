#include "../include/imunitet.h"


Imunitet::Imunitet(int redniBroj, const QList<Igrac*> &igraci)
    : Polje(redniBroj, igraci)
{
}

Imunitet::~Imunitet()
{

}

int Imunitet::akcija()
{
    Igrac* igrac = prikaziIgracaNaPolju();
    igrac->dodajZivot();

    return 0;
}

//Imunitet::~Imunitet(){

//}
