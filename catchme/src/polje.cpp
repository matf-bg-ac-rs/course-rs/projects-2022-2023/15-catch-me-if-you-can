#include "../include/polje.h"



Polje::Polje(int redniBroj, const QList<Igrac*> &igraci)
    : _redniBroj(redniBroj),
      _igraci(igraci)
{}

Polje::~Polje()
{

}


Polje::Polje(int redniBroj): _redniBroj(redniBroj)
{
    QList<int> igraci;
}

int Polje::akcija()
{
    return 0;
}

void Polje::staviIgracaNaPolje(Igrac* i)
{
    _igraci.append(i);
}

void Polje::ukloniIgracaSaPolja()
{
    if(brojIgraca()>0)
        _igraci.removeFirst();
}

Igrac* Polje::prikaziIgracaNaPolju() const
{
    if(_igraci.size()>0){
    return _igraci[0];
    }
    else{
    return nullptr;
    }
}

Igrac* Polje::prikaziPoslednjeg() const
{
    if(_igraci.size()>0){
    return _igraci.last();
    }
    else{
    return nullptr;
    }

}

int Polje::brojIgraca() const
{
    return _igraci.size();
}

int Polje::redniBroj() const
{
    return _redniBroj;
}

//Polje Polje::dohvatiPolje(int redniBroj) const
//{
//    return *this;
//}


