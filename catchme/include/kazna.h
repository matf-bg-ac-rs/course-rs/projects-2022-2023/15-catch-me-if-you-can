#ifndef KAZNA_H
#define KAZNA_H
#include "polje.h"


class Kazna : public Polje
{
public:
    Kazna(int redniBroj, const QList<Igrac*> &igraci);
    ~Kazna() override;
    int akcija()  override;
};

#endif // KAZNA_H


