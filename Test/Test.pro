QT = core \
    network \
    gui \
    widgets

TEMPLATE = app

CONFIG += c++17 \
        console

HEADERS += \
    catch.hpp \
    ../catchme/include/polje.h \
    ../catchme/include/igrac.h \
    ../catchme/include/enumboje.h\
    ../catchme/include/kviz.h\
    ../catchme/include/pitanje.h\
    ../catchme/include/kvizwindow.h\
    ../catchme/include/spisakpitanja.h\
    ../catchme/include/imunitet.h\
    ../catchme/include/kocka.h\
    ../catchme/include/tunel.h\
    ../catchme/include/lopta.h\
    ../catchme/include/breakoutgame.h\
    ../catchme/include/blok.h\
    ../catchme/include/reket.h\
    ../catchme/include/mainwindow.h\
    ../catchme/include/enummode.h\
    ../catchme/include/enumshade.h\
    ../catchme/include/enumboje.h\
    ../catchme/include/kazna.h\
    ../catchme/include/nagrada.h\
    ../catchme/include/zatvor.h\
    ../catchme/include/prikazpobednika.h\
    ../catchme/include/igrica.h\
    ../catchme/include/miniigrazmijica.h\
    ../catchme/include/pogodibroj.h\
    ../catchme/include/partija.h\



SOURCES += \
    IgracTest.cpp \
    ImunitetTest.cpp \
    KaznaTest.cpp \
    KockaTest.cpp \
    KvizTest.cpp \
    LoptaTest.cpp \
    NagradaTest.cpp \
    PitanjeTest.cpp \
    PoljeTest.cpp \
    SpisakPitanjaTest.cpp \
    TunelTest.cpp \
    ZatvorTest.cpp \
    test.cpp \
    ../catchme/src/igrac.cpp\
    ../catchme/src/polje.cpp\
    ../catchme/src/kviz.cpp\
    ../catchme/src/pitanje.cpp\
    ../catchme/src/spisakpitanja.cpp\
    ../catchme/src/kvizwindow.cpp\
    ../catchme/src/imunitet.cpp\
    ../catchme/src/kocka.cpp\
    ../catchme/src/tunel.cpp\
    ../catchme/src/lopta.cpp\
    ../catchme/src/breakoutgame.cpp\
    ../catchme/src/reket.cpp\
    ../catchme/src/blok.cpp\
    ../catchme/src/mainwindow.cpp\
    ../catchme/src/kazna.cpp\
    ../catchme/src/nagrada.cpp\
    ../catchme/src/zatvor.cpp\
    ../catchme/src/prikazpobednika.cpp\
    ../catchme/src/igrica.cpp\
    ../catchme/src/miniigrazmijica.cpp\
    ../catchme/src/pogodibroj.cpp\
    ../catchme/src/partija.cpp\




FORMS +=\
    ../catchme/forms/kvizwindow.ui\
    ../catchme/forms/breakoutgame.ui\
    ../catchme/forms/mainwindow.ui\
    ../catchme/forms/partija.ui\
    ../catchme/forms/prikazpobednika.ui\
    ../catchme/forms/miniigrazmijica.ui\
    ../catchme/forms/pogodibroj.ui\






TARGET = Test

DISTFILES +=
