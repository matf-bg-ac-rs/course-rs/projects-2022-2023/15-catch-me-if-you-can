#include "../include/miniigrazmijica.h"
#include "ui_miniigrazmijica.h"

MiniIgraZmijica::MiniIgraZmijica(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MiniIgraZmijica)
{
    ui->setupUi(this);

    scene = new QGraphicsScene(0,0,800,600);

    ui->graphicsView->setScene(scene);
    //ui->graphicsView->setInteractive(true);
    //ui->graphicsView->scene()->setFocusItem(_glava);

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(close()));
    //QGraphicsScene::connect(timer, SIGNAL(timeout()), scene, SLOT(close()));

    timer->start(15000);

    start();
}

void MiniIgraZmijica::start()
{
    startGlava();
    startVocka();
    startTelo();
}

void MiniIgraZmijica::startGlava()
{
    _glava = new QGraphicsRectItem();
    scene->addItem(_glava);
    _glava->setFocus();
    ui->graphicsView->scene()->focusItem();

    _glava->setRect(0,0,50,50);
    _glavaBrush.setStyle(Qt::SolidPattern);
    _glavaBrush.setColor(Qt::blue);
    _glava->setBrush(_glavaBrush);
    _glava->setFlag(QGraphicsItem::ItemIsFocusable,true);

    //_zmijaTela.prepend(_glava);
}

void MiniIgraZmijica::startVocka()
{
    _vocka = new QGraphicsEllipseItem();
    _vocka->setPos(300,300);
    scene->addItem(_vocka);

    _vocka->setFlag(QGraphicsItem::ItemIsFocusable,true);
    _vocka->setRect(0,0,50,50);
    _vockaBrush.setStyle(Qt::SolidPattern);
    _vockaBrush.setColor(Qt::red);
    _vocka->setBrush(_vockaBrush);
}

void MiniIgraZmijica::startTelo()
{
    _telo = new QGraphicsRectItem();
    _telo->setPos(0,50);
    scene->addItem(_telo);
    _zmijaTela.prepend(_telo);

    _telo->setFlag(QGraphicsItem::ItemIsFocusable,true);
    _telo->setRect(0,0,50,50);
    _teloBrush.setStyle(Qt::SolidPattern);
    _teloBrush.setColor(Qt::cyan);
    _telo->setBrush(_teloBrush);

}

void MiniIgraZmijica::keyPressEvent(QKeyEvent *event)
{
    //ui->graphicsView->keyboardGrabber()

    if (event->key() == Qt::Key_W){
        _pozPre = _glava->pos();
        int xPos = _glava->x();
        int yPos = _glava->y() - _glava->boundingRect().height();
        _glava->setPos(xPos,yPos);
        pomeriTela();
    }

    if (event->key() == Qt::Key_S){
        _pozPre = _glava->pos();
        int xPos = _glava->x();
        int yPos = _glava->y() + _glava->boundingRect().height();
        _glava->setPos(xPos,yPos);
        pomeriTela();
    }

    if (event->key() == Qt::Key_D){
        _pozPre = _glava->pos();
        int xPos = _glava->x() + _glava->boundingRect().width();
        int yPos = _glava->y();
        _glava->setPos(xPos,yPos);
        pomeriTela();
    }

    if (event->key() == Qt::Key_A){
        _pozPre = _glava->pos();
        int xPos = _glava->x() - _glava->boundingRect().width();
        int yPos = _glava->y();
        _glava->setPos(xPos,yPos);
        pomeriTela();
    }

    QList<QGraphicsItem*> cItems = _glava->collidingItems();
    for (size_t i = 0, n = cItems.size(); i < n; ++i){
        if (typeid(*cItems[i]) == typeid(*_vocka)){
            elongate();
            _duzina++;
        }
    }

    //QDialog::keyPressEvent(event);
    //QGraphicsScene::keyPressEvent(event);

}

void MiniIgraZmijica::elongate()
{
    QGraphicsRectItem* novoTelo = new QGraphicsRectItem();
    novoTelo->setRect(0,0,50,50);
    novoTelo->setBrush(_teloBrush);

    _zmijaTela.prepend(novoTelo);
    novoTelo->setPos(-200,-200);
    scene->addItem(novoTelo);

    int x = rand() % 600;
    int y = rand() % 600;

    _vocka->setPos(x, y);
}

void MiniIgraZmijica::pomeriTela()
{
    for (size_t i = 0, n = _zmijaTela.size()-1; i < n; ++i){
        _zmijaTela[i]->setPos(_zmijaTela[i+1]->pos());
    }

    _zmijaTela.last()->setPos(_pozPre);
}

int MiniIgraZmijica::getPomerajAkcija()
{
    if (_duzina > 5) {
        _pomerajAkcija = 1;
    } else {
        _pomerajAkcija = -1;
    }

    return _pomerajAkcija;
}

MiniIgraZmijica::~MiniIgraZmijica()
{
    delete ui;
}

