QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/miniigrazmijica.cpp \
    src/igrica.cpp \
    src/prikazpobednika.cpp \
    src/pogodibroj.cpp \
    src/blok.cpp \
    src/breakoutgame.cpp \
    src/kvizwindow.cpp \
    src/igrac.cpp \
    src/imunitet.cpp \
    src/kazna.cpp \
    src/kocka.cpp \
    src/kviz.cpp \
    src/lopta.cpp \
    src/nagrada.cpp \
    src/partija.cpp \
    src/pitanje.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/polje.cpp \
    src/reket.cpp \
    src/spisakpitanja.cpp \
    src/tunel.cpp \
    src/zatvor.cpp \


HEADERS += \
    include/enummode.h \
    include/blok.h \
    include/breakoutgame.h \
    include/enumboje.h \
    include/enumshade.h \
    include/igrac.h \
    include/igrica.h \
    include/imunitet.h \
    include/kazna.h \
    include/kocka.h \
    include/kviz.h \
    include/lopta.h \
    include/mainwindow.h \
    include/nagrada.h \
    include/partija.h \
    include/pitanje.h \
    include/polje.h \
    include/reket.h \
    include/spisakpitanja.h \
    include/tunel.h \
    include/zatvor.h \
    include/kvizwindow.h \
    include/pogodibroj.h \
    include/prikazpobednika.h \
    include/miniigrazmijica.h


FORMS += \
    forms/breakoutgame.ui \
    forms/mainwindow.ui \
    forms/partija.ui \
    forms/kvizwindow.ui \
    forms/pogodibroj.ui \
    forms/prikazpobednika.ui \
    forms/miniigrazmijica.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

DISTFILES +=
