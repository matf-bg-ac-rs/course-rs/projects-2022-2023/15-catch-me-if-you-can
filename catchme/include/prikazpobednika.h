#ifndef PRIKAZPOBEDNIKA_H
#define PRIKAZPOBEDNIKA_H

#include <QLabel>
#include <QWidget>
#include "igrac.h"


namespace Ui {
class PrikazPobednika;
}

class PrikazPobednika : public QWidget
{
    Q_OBJECT

public:
    explicit PrikazPobednika(Igrac* pobednik, QList<Igrac*> igraci, QWidget *parent = nullptr);
    ~PrikazPobednika();
    void inicijalizujListuIgraca();

private:
    Ui::PrikazPobednika *ui;

    Igrac* _pobednik;

    QList<Igrac*> _igraci;
    QList<QLabel*> _labelIgraca;
    QList<QLabel*> _labelPijuna;

};

#endif // PRIKAZPOBEDNIKA_H
