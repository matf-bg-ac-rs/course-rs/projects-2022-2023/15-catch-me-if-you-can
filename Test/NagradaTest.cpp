#include <QDebug>
#include "../catchme/include/nagrada.h"
#include "../catchme/include/igrac.h"

#include "catch.hpp"


TEST_CASE("klasaNagrada","[class]"){

    SECTION("konstruktor postoji")
    {

        // Arrange
        int redniBrPolja=12;
        QString ime = "nina";
        Igrac* igrac = new Igrac(0,ime,1,Boja::Crna,0);
        QList<Igrac*> igraci={igrac};

        //Act
        Nagrada* n = new Nagrada(redniBrPolja, igraci);

        // Assert

        REQUIRE_NOTHROW(n);
    }


    SECTION("Akcija vraca vrednost -1")
    {

        // Arrange
        int redniBrPolja=12;
        QString ime = "nina";
        Igrac* igrac = new Igrac(0,ime,1,Boja::Crna,0);
        QList<Igrac*> igraci={igrac};
        Nagrada* n = new Nagrada(redniBrPolja, igraci);
        int ocekivano = 1;

         // Act
        int rezultat = n->akcija();

        // Assert

         REQUIRE(ocekivano == rezultat);


    }

    SECTION("Desktruktor postoji")
    {
        // Arrange
        QList<Igrac*> igraci=QList<Igrac*>();
        int redniBrPolja=16;
        Nagrada* n = new Nagrada(redniBrPolja, igraci);

        //Act
        delete n;

        // Assert
        REQUIRE_NOTHROW(n);
    }

}
