#include "../include/mainwindow.h"
#include "../include/partija.h"
#include "../include/breakoutgame.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QMessageBox>

#define MAX_BR_IGRACA 5
#define MIN_BR_IGRACA 2

BreakoutGame* _breakout;

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("catchme");
    //this->setFixedSize(this->width(), this->height());
    this->setStyleSheet(
                "#StranaPoceniMeni{ "
                " border-image: url(:/resources/images/background.png) 0 0 0 0 stretch stretch;"
                "}"
                "#StranaPodesavanja{ "
                " border-image: url(:/resources/images/background.png) 0 0 0 0 stretch stretch;"
                "}"
                "#StranaPartija{ "
                " border-image: url(:/resources/images/background.png) 0 0 0 0 stretch stretch;"
                "}"
                "#StranaPobednik{ "
                " border-image: url(:/resources/images/background.png) 0 0 0 0 stretch stretch;"
                "}"
                "#StranaPravilaIgre{ "
                " border-image: url(:/resources/images/background.png) 0 0 0 0 stretch stretch;"
                "}"
                "#StranaPodesavanjaIgraca{ "
                " border-image: url(:/resources/images/background.png) 0 0 0 0 stretch stretch;"
                "}"
                )
            ;

    partija=new Partija();
    ui->stackedWidget->setCurrentIndex(0);

    _breakout = new BreakoutGame();
    connect(partija, SIGNAL(pozoviBreakoutPromenjeno()), this, SLOT(odigrajAkciju()));
    //connect(dynamic_cast<Igrica *>(partija->polja()[6]), SIGNAL(pozoviBreakoutPromenjeno()), this, SLOT(odigrajAkciju()));
    //QTimer timer;
    QObject::connect(&timer, &QTimer::timeout, _breakout->scena, &QGraphicsScene::advance);

}

// stackedWidget 0 je main meni, 5 je pravila igre, 4 je podesi igrace
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btZapocniPartiju_clicked()
{
    partija->setBrojIgraca(_brojIgraca);
    partija->setImena(_imena);
    partija->setBoje(_boje);

    partija->podesiProzor();

    partija->show();
    this->hide();
}

void MainWindow::on_btPravilaIgre_clicked()
{
    ui->stackedWidget->setCurrentIndex(5);
}

void MainWindow::on_btNazad_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::on_btNapustiIgru_clicked()
{
    qApp->exit();
}

void MainWindow::on_btPodesiIgrace_clicked()
{
    ui->stackedWidget->setCurrentIndex(4);
}

void MainWindow::on_btPodesavanja_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_pushButton_clicked() //dugme nazad kod podesavanja
{
    ui->stackedWidget->setCurrentIndex(static_cast<int>(Mode::Easy));
    int indeks=ui->cbMode->currentIndex();

    if (indeks==static_cast<int>(Mode::Easy))
        _tezina=Mode::Easy;  
    else if (indeks==static_cast<int>(Mode::Medium))
        _tezina=Mode::Medium; 
    else
        _tezina=Mode::Hard;

    podesiShade();
    podesiMode();
}

//--------DEO VEZAN ZA STRANU PODESAVANJA IGRACA---------------------
void MainWindow::on_pbPotvrdi_clicked()
{
    izdvojBrojIgraca();
    izdvojImenaIgraca();
    izdvojBojeIgraca();

    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::izdvojBrojIgraca()
{
    bool ok;
    const auto brIgraca=ui->lbBroIgraca->text().toInt(&ok);

    if(!ok || brIgraca<MIN_BR_IGRACA || brIgraca>MAX_BR_IGRACA)
    {
        QMessageBox msgBox;
        msgBox.setText("Morate uneti izmedju 2 i 5 igraca");
        msgBox.exec();
    }

    if(ok)
        setBrojIgraca(brIgraca);
    else
        setBrojIgraca(MIN_BR_IGRACA);
}

Boja MainWindow::vratiBoju(int idx) {

    if (idx == static_cast<int>(Boja::Roze)) {
        return Boja::Roze;
    } else if (idx == static_cast<int>(Boja::Crvena)) {
        return Boja::Crvena;
    } else if (idx == static_cast<int>(Boja::Siva)) {
        return Boja::Siva;
    } else if (idx == static_cast<int>(Boja::Crna)) {
        return Boja::Crna;
    } else {
        return Boja::Plava;
    }
}

QList<QLineEdit *> MainWindow::napraviListuLabelaImena()
{
    QList<QLineEdit*> labeleImena;

    labeleImena.append(ui->lbImeIgraca1);
    labeleImena.append(ui->lbImeIgraca2);
    labeleImena.append(ui->lbImeIgraca3);
    labeleImena.append(ui->lbImeIgraca4);
    labeleImena.append(ui->lbImeIgraca5);

    return labeleImena;
}

void MainWindow::izdvojImenaIgraca(){

    QList<QLineEdit*> labeleImena=napraviListuLabelaImena();
    _imena.clear();

    for(int i=0;i<MAX_BR_IGRACA;i++){
        auto ime=labeleImena[i]->text();
        if(!ime.compare("")){
            ime=QString("Igrac "+QString::number(i+1));
        }
        _imena.append(ime);
    }
}

QList<QComboBox *> MainWindow::napraviListuOdabiraBoje()
{
    QList<QComboBox*>listaOdabiraBoje;

    listaOdabiraBoje.append(ui->cbBoja1);
    listaOdabiraBoje.append(ui->cbBoja2);
    listaOdabiraBoje.append(ui->cbBoja3);
    listaOdabiraBoje.append(ui->cbBoja4);
    listaOdabiraBoje.append(ui->cbBoja5);

    return listaOdabiraBoje;
}

void MainWindow::izdvojBojeIgraca()
{
    QList<QComboBox*>listaOdabiraBoje=napraviListuOdabiraBoje();
    _boje.clear();

    for(int i=0;i<MAX_BR_IGRACA;i++){
        int idx=listaOdabiraBoje[i]->currentIndex();
        Boja b=vratiBoju(idx);
        _boje.append(b);
    }
}

void MainWindow::podesiMode()
{
    partija->setMode(_tezina);
}

void MainWindow::podesiShade()
{
    int indeks=ui->cbShade->currentIndex();

    if (indeks==static_cast<int>(Shade::light))
        partija->setShade(Shade::light);   
    else
       partija->setShade(Shade::dark);

    ui->stackedWidget->setCurrentIndex(0);
}

void MainWindow::odigrajAkciju()
{
    _breakout->restartuj();
    //_breakout->_akcija = 1;
    _breakout->show();
//    QTimer timer;

    // Postavljamo da se advance poziva na svakih 1000/30 ms, sto daje 30fps.
    timer.start(1000/60);

}

void MainWindow::setBrojIgraca(int newBrojIgraca)
{
    _brojIgraca = newBrojIgraca;
}

int MainWindow::brojIgraca() const
{
    return _brojIgraca;
}
//-----KRAJ DELA ZA STRANU SA PODESAVANJIMA IGRACA----------------







