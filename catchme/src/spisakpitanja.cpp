#include "../include/spisakpitanja.h"


SpisakPitanja::SpisakPitanja()
{

    //mode EASY (uz par izuzetaka)
    Pitanje* p1 = new Pitanje("Ko je pobedio na Evrvoziji 2022.godine?" ,   {"Srbija", "Ukrajina", "Australija"}, 1, Mode::Easy);
    Pitanje* p2 = new Pitanje("Koja je boja azurna?" ,   {"bela", "ljubicasta", "plava"}, 2, Mode::Easy);
    Pitanje* p3 = new Pitanje("Nauka o celiji je?" ,   {"miktobiologija", "citologija", "homologija"}, 1, Mode::Easy);
    Pitanje* p4 = new Pitanje("Kloniranje je?" ,   {"stvaranje biološkog dvojnika", "izrada lažnog novca", "improvizacija na osnovu postojećeg melodijskog obrasca"}, 0, Mode::Easy);
    Pitanje* p5 = new Pitanje("Boginja lepote i ljubavi kod starih Grka zvala se?" ,   {"Gea", "Hera", "Afrodita"}, 2, Mode::Easy);
    Pitanje* p6 = new Pitanje("Ispravno je napisati: " ,   {"malo pre", "malopre", "pravilno je i jedno i drugo, u zavisnosti od konteksta"}, 2, Mode::Easy);
    Pitanje* p7 = new Pitanje("Rumunija se ne graniči sa: " ,   {"Ukrajinom", "Moldavijom", "Hrvatskom"}, 2, Mode::Easy);
    Pitanje* p8 = new Pitanje("Tokom evolucije, najblizi preci ptica bili su: " ,   {"gmizavci", " papkari ", " kljunari"}, 0, Mode::Medium);
    Pitanje* p9 = new Pitanje("Protagonist je: " ,   {"glavna ličnost ", "nametljivac", "naučnik"}, 0, Mode::Easy);
    Pitanje* p10 = new Pitanje("Sta je 'bela kuga'?" ,   {"osiromasenje građana ", "sve česća pojava trovanja lekovima", "smanjenje nataliteta"}, 2, Mode::Easy);
    Pitanje* p11 = new Pitanje("Kako se pise?" ,   {"predsednik", "pretsednik", "presednik"}, 0, Mode::Easy);
    Pitanje* p12 = new Pitanje( "Sa kojom od navedenih država ne graniči Austrija ?" , {"Slovacka", "Poljska", "Ceska"}, 1, Mode::Easy);
    Pitanje* p13= new Pitanje("Koja je najprodavanija konzola svih vremena?" , {"PlayStation 2", "Super Nintendo", "Play Station 4"},0 , Mode::Easy );
    Pitanje* p14= new Pitanje("Za šta od navedenog je najbolje koristiti tip podataka Enum?" , {"Adrese", "Strane svete", "Prosecnu ocenu"}, 1, Mode::Easy);
    Pitanje* p15= new Pitanje("Za šta od navedenog je najbolje koristiti niz?", {"Starost", "Korisnicko ime", "Pesme u albumu"}, 2, Mode::Easy);
    Pitanje* p16= new Pitanje("Kom članu klase se ne može pristupiti iz izvedene klase?", {"Javnoj metodi", "Zasticenoj metodi", "Privatnoj metodi"},2, Mode::Easy  );
    Pitanje* p17 = new Pitanje("Koja struktura od ponuđenih koristi FIFO metod?" ,   {"red ", "stek", "hes tabela"}, 0, Mode::Easy);
    Pitanje* p18 = new Pitanje("Šta od ponuđenog nije operativni sistem? " ,   {"Android ", "MacOS", "Bash"}, 2, Mode::Easy);
    Pitanje* p19 = new Pitanje("Ruteri prosleđuju podatke između uređaja na osnovu: " ,   {"IP adresa", "Domena", "MAC adresa"}, 0, Mode::Medium);
    Pitanje* p20 = new Pitanje("Hakerski napad u kome obmanete osobu da vam sama kaže PIN je poznat kao: " ,   {"Kradja identiteta ", "Social engineering", "Relationship hacking"}, 1, Mode::Medium);
    Pitanje* p21 = new Pitanje("Sta ne predstavlja par suprotnosti? " ,   {"altruista - egoista  ", "neskladan -disharmoničan", "jednostavan - kompleksan"}, 1, Mode::Easy);
    Pitanje* p22 = new Pitanje("Kad se kaze da je neka biljna vrsta autohtona, to znači da je ona: " ,   {"samonikla", "dragocena", "istrebljena"}, 0, Mode::Easy);
    Pitanje* p23 = new Pitanje("Akreditiv je: " ,   {"punomoćje ", "padeski oblik", "pozajmica"}, 0, Mode::Hard);

    _spisakPitanja.push_back(p1);
    _spisakPitanja.push_back(p2);
    _spisakPitanja.push_back(p3);
    _spisakPitanja.push_back(p4);
    _spisakPitanja.push_back(p5);
    _spisakPitanja.push_back(p6);
    _spisakPitanja.push_back(p7);
    _spisakPitanja.push_back(p8);
    _spisakPitanja.push_back(p9);
    _spisakPitanja.push_back(p10);
    _spisakPitanja.push_back(p11);
    _spisakPitanja.push_back(p12);
    _spisakPitanja.push_back(p13);
    _spisakPitanja.push_back(p14);
    _spisakPitanja.push_back(p15);
    _spisakPitanja.push_back(p16);
    _spisakPitanja.push_back(p17);
    _spisakPitanja.push_back(p18);
    _spisakPitanja.push_back(p19);
    _spisakPitanja.push_back(p20);
    _spisakPitanja.push_back(p21);
    _spisakPitanja.push_back(p22);
    _spisakPitanja.push_back(p23);


    // mode MEDIUM (uz par izuzetaka)
    Pitanje* p24 = new Pitanje("Koji je glavni grad Ausrtalije" ,  {"Sidnej", "Kanbera", "Singapur"}, 0, Mode::Easy);
    Pitanje* p25= new Pitanje("Koji procenat teritorije Egipta zauzima pustinja", {"96%", "76%", "86"}, 0, Mode::Medium);
    Pitanje* p26= new Pitanje("Šta je FTP?" , {"Protokol za upravljanje štampačem" , "Standard za enkripciju" , "Standard za slanje mejlova"} , 0, Mode::Medium);
    Pitanje* p27= new Pitanje("Piter Džekson je 2005. godine uradio rimejk poznatog filmskog klasika??" , {"King Kong" , "Psiho" , "Carobnjak iz Oza"} , 0, Mode::Medium);
    Pitanje* p28= new Pitanje("Šta vas štiti od 'man in the middle' napada?" , {"https" , "FTP" , "iPhone"} , 0, Mode::Medium);
    Pitanje* p29= new Pitanje("Koju HTTP metodu bi koristio za logovanje korisnika na sistem?" , {"GET" , "POST" , "LOGIN"} , 1, Mode::Medium);
    Pitanje* p30= new Pitanje("Šta je izuzetak?" , {"Sintaksni problem" , "Problem nastao tokom izvršavanja" , "Problem nastao tokom kompajliranja"} , 1, Mode::Easy);
    Pitanje* p31= new Pitanje("Šta je HTTP metoda?" , {"GET" , "POST" , "CANCLE"} ,2 , Mode::Hard);
    Pitanje* p32= new Pitanje("Imam osam metoda uključujući CONNECT, PATCH, PUT i POST. I nemam stanja. Šta sam ja?" , {"CPU" , "Postar" , "HTTP protokol"} , 2, Mode::Medium);
    Pitanje* p33= new Pitanje("Šta znači skraćenica LAN?" , {"Live Adaptable Network" , "Local Area Network" , "Local Alternative Network"} , 1, Mode::Medium);
    Pitanje* p34= new Pitanje("Koji HTML element se koristi za definisanje internih stilova?" , {"<style>" , "<design>" , "<css>"} , 0, Mode::Medium);
    Pitanje* p35= new Pitanje("Koja je podrazumevana vrednost 'position' CSS svojstva?" , {"position: static" , "position: relative" , "position: absolute"} , 0, Mode::Medium);
    Pitanje* p36= new Pitanje("Koji HTML atribut se koristi da bi se prikazao tekst kada slika ne može da se učita?" , {"alt" , "src" , "longdesc"} , 0, Mode::Easy);
    Pitanje* p37= new Pitanje("WYSIWYG je skraćenica za?" , {"What You See Is What You Gain","Where You Start IS Where You Get" , "What You See Is What You Get"} , 2, Mode::Medium);
    Pitanje* p38= new Pitanje("Koja je podrazumevana vrednost 'display' svojstva za <div> element?", {"display:block", "display:inline", "display:none"}, 1, Mode::Medium);
    Pitanje* p39= new Pitanje("Izabrati ispravno ispisan HTML5 element:" , {"<input type='text'>" , "<input='text'>" , "<input type=text>"} , 0, Mode::Medium);

    _spisakPitanja.push_back(p24);
    _spisakPitanja.push_back(p25);
    _spisakPitanja.push_back(p26);
    _spisakPitanja.push_back(p27);
    _spisakPitanja.push_back(p28);
    _spisakPitanja.push_back(p29);
    _spisakPitanja.push_back(p30);
    _spisakPitanja.push_back(p31);
    _spisakPitanja.push_back(p32);
    _spisakPitanja.push_back(p33);
    _spisakPitanja.push_back(p34);
    _spisakPitanja.push_back(p35);
    _spisakPitanja.push_back(p36);
    _spisakPitanja.push_back(p37);
    _spisakPitanja.push_back(p38);
    _spisakPitanja.push_back(p39);

    //mode HARD (uz par izuzetaka)
    Pitanje* p40 = new Pitanje("Dijapazon je: " ,  {"geometrijsko telo", "antički bog igre", "opseg "}, 2, Mode::Hard);
    Pitanje* p41= new Pitanje("Kako se zvao otac  Stefana Dusana?" , {"Stefan Uros II Milutin" , "Stefan Uros III Decanski" , "Stefan Dragutin"} , 1, Mode::Hard);
    Pitanje* p42= new Pitanje("U bici kod Farsale Cezar je pobijedio ?", {"Marka Antonija", "Pompeja" , "Kleopatru"}, 1, Mode::Medium);
    Pitanje* p43 = new Pitanje("Korisničke lozinke u bazi treba čuvati:" ,  {"Ne treba čuvati uopšte", "U zasebnoj tabeli", "Hešovane"}, 2, Mode::Hard);
    Pitanje* p44 = new Pitanje("Koju komandu koristimo kada želimo da dohvatimo podatke iz baze? " ,  {"EXTRACT", "SELECT", "GET "}, 1, Mode::Hard);
    Pitanje* p45 = new Pitanje("HTTP kolačići se koriste za: " ,  {"Čuvanje stanja između uzastopnih HTTP zahteva", "Čuvanje istorije veb pretrage", "Logovanje korisnika na operativni sistem"}, 0, Mode::Hard);
    Pitanje* p46 = new Pitanje("Za šta programeri najčešće koriste AJAX?" ,  {"Asinhrono slanje/primanje podataka", "Prikaz rezultata", "Pranje monitora"}, 0, Mode::Hard);
    Pitanje* p47 = new Pitanje("Šta se od navedenog koristi da se vaše korisničko ime i lozinka bezbedno prenesu do servera? " ,  {"HTTPS", "Proxy", "DNS "}, 0, Mode::Hard);
    Pitanje* p48 = new Pitanje("Ja imam funkcije PUSH i POP. Ko sam ja? " ,  {"Stablo", "Hes tabela", "stek "}, 2, Mode::Hard);
    Pitanje* p49 = new Pitanje("Koja izjava o interfejsima je netačna? " ,  {"Interfejs može naslediti apstraktnu klasu", "Nije moguće kreirati instancu interfejsa", "Interfejs ne može sadržati implementacije metoda "}, 0, Mode::Hard);
    Pitanje* p50 = new Pitanje("Koji mehanizam štiti od lažiranja imejl adrese (email spoofing)?" ,  {"enkripcija", "SPF", "HTTPS"}, 2, Mode::Hard);
    Pitanje* p51 = new Pitanje("Koji od navedenih nije tip JOIN-a u SQL-u? " ,  {"INNER", "RIGHT", "AROUND"}, 2, Mode::Hard);
    Pitanje* p52 = new Pitanje("Koliko bitova se koristi za pamćenje IPv6 adrese? " ,  {"12", "128", "1024 "}, 1, Mode::Hard);
    Pitanje* p53 = new Pitanje("Šta od navedenog je algoritam za asimetričnu enkripciju? " ,  {"RSA", "MAC", "DES "}, 0, Mode::Hard);
    Pitanje* p54 = new Pitanje("Ja sadržim potpise metoda, ali ne implementaciju. Ko sam ja? " ,  {"apstraktna klasa", "interfejs", "objekat "}, 1, Mode::Hard);
    Pitanje* p55 = new Pitanje("Šta se koristi da bi se internet domen mapirao u IP adresu? " ,  {"DNS", "DDoS", "ARP "}, 0, Mode::Hard);
    Pitanje* p56 = new Pitanje("Kako izgleda HTTP kod koji vraća server kada je zahtev uspešno izvršen? " ,  {"4xx", "2xx", "6xx "}, 1, Mode::Hard);
    Pitanje* p57= new Pitanje("Šta znači skraćenica CSS? " , {"Cascading Style Sheets" , "Common Styled Symbols" , "Computer Style Sheets"} , 0, Mode::Medium);
    Pitanje* p58= new Pitanje("Za šta služi <doctype> u HTML-u?" , {"Dodaje CSS stilove na stranicu" , "Standard za enkripciju" , "Govori pregledaču koja verzija HTML-a se koristi"} , 2, Mode::Medium);


    _spisakPitanja.push_back(p40);
    _spisakPitanja.push_back(p41);
    _spisakPitanja.push_back(p42);
    _spisakPitanja.push_back(p43);
    _spisakPitanja.push_back(p44);
    _spisakPitanja.push_back(p45);
    _spisakPitanja.push_back(p46);
    _spisakPitanja.push_back(p47);
    _spisakPitanja.push_back(p48);
    _spisakPitanja.push_back(p49);
    _spisakPitanja.push_back(p50);
    _spisakPitanja.push_back(p51);
    _spisakPitanja.push_back(p52);
    _spisakPitanja.push_back(p53);
    _spisakPitanja.push_back(p54);
    _spisakPitanja.push_back(p55);
    _spisakPitanja.push_back(p56);
    _spisakPitanja.push_back(p57);
    _spisakPitanja.push_back(p58);



}

SpisakPitanja::~SpisakPitanja()
{

}



Pitanje* SpisakPitanja::izvuciRandomPitanje(Mode mode)
{
    QList<Pitanje *> spisakPitanja=filtrirajSpisakPitanja(mode);

    int rand_index = rand() % spisakPitanja.length();
    return spisakPitanja[rand_index];
}


QList<Pitanje* > SpisakPitanja::filtrirajSpisakPitanja(Mode mode) {
    QList<Pitanje *> spisakPitanja;
    for(Pitanje *p : _spisakPitanja){
        if(p->mode() == mode){
            spisakPitanja.push_back(p);
        }
    }
    return spisakPitanja;

}

