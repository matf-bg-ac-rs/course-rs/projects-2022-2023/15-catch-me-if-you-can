Kratak opis: Po zavrsetku odigrane partije, aplikacija obavestava o pobedniku: otvara se novi prozor na kom pise ime igraca koji je prvi stao na zavrsno polje i njegove karakteristike.
	Kad se prikaze pobednik, igrac ima opciju da ugasi prozor, a zatim da zapocne novu partiju ili u napusti aplikaciju.

Akteri: Igrac

Preduslovi: Uspesno izvrsena partija.
Postuslovi: Prikazan je pobednik.

Osnovni tok:
	1. Otvara se prozor sa nazivom pobednika.
	2. Igrac klikne na dugme "zatvori"
	3. Igrac ima 2 opcije:
		3.1. Klikom na dugme "ponovo", zapocinje novu partiju.
		3.2. Klikom na dugme "kraj", izlazi iz aplikacije.

Alternativni tokovi:
	A1: Neocekivani izlaz iz aplikacije. Ako korisnik bilo kada u toku prikaza pobednika iskljuci apllikaciju, informacije se gube i aplikacija zavrsava rad. Slucaj upotrebe se zavrsava.

Podtokovi: /

Specijalni zahtevi:/

Dodatne info: / 