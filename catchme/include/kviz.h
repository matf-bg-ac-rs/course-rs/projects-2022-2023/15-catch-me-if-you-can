#ifndef KVIZ_H
#define KVIZ_H

#include "polje.h"
#include "pitanje.h"
#include "spisakpitanja.h"
#include "../include/kvizwindow.h"
#include "../include/enummode.h"

#include <QVector>

class Kviz : public Polje{
public:
    Kviz(int redniBroj, const QList<Igrac*> &igraci, int korisnikovOdgovor);
    ~Kviz() override;

    int akcija()   override;
    void izvuci_pitanje(Mode mode);

private:
    SpisakPitanja* _pitanja;
    int _korisnikovOdgovor;
    Pitanje* _izvucenoPitanje;
    kvizWindow* _kvizWindow;

};

#endif // KVIZ_H
