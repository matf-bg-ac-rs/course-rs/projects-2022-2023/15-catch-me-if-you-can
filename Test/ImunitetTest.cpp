#include <QDebug>
#include "../catchme/include/imunitet.h"
#include "../catchme/include/igrac.h"

#include "catch.hpp"


TEST_CASE("Imunitet","[class]"){

    SECTION("konstruktor postoji")
           {

        // Arrange
        int redniBrPolja=3;
        QList<Igrac*> igraci=QList<Igrac*>();
        Imunitet* i = new Imunitet(redniBrPolja, igraci);

        // Assert

        REQUIRE_NOTHROW(i);
    }


    SECTION("Igrac koji stane na polje imunitet dobija 1 zivot")
           {

        // Arrange
        QString ime = "Milos";
        Boja boja=Boja::Crna;
        Igrac* igrac = new Igrac(5, ime, 2, boja, 14);  //proveri polje
        int ocekivaniBrojZivota = 3;

         // Act
        igrac->dodajZivot();
        int dobijenBrojZivota = igrac->getBrojZivota();

        // Assert

         REQUIRE(ocekivaniBrojZivota == dobijenBrojZivota);


           }

    SECTION("Desktruktor postoji")
            {
                // Arrange
                QList<Igrac*> igraci=QList<Igrac*>();
                Imunitet* imunitet = new Imunitet(3, igraci);
                delete imunitet;

                // Assert
                REQUIRE_NOTHROW(imunitet);
            }




}
