#ifndef IGRAC_H
#define IGRAC_H

#include <QString>
//#include <QGraphicsItem>

#include "enumboje.h"


class Igrac
{
public:
    Igrac();
    Igrac(unsigned int id,  QString &ime, Boja boja);
    Igrac(unsigned int id, const QString &ime, int brojZivota, Boja boja, int redniBrojPolja);
    ~Igrac();
public:
    int getId() const;
    int getBrojZivota() const;
    int getRedniBrojPolja() const;
    void setRedniBrojPolja(int i);
    void setPozadina(const Boja boja);
    void dodajZivot();
    void oduzmiZivot();
    void ubaciUZator();
    void izbaciIzZatvora();
    bool uZatvoru() const;

    QString getPozadina() const;

    QString getIme() const;

    Boja getBoja() const;



private:
    unsigned _id;
    QString _ime;
    bool _naRedu;
    int _brojZivota;
    Boja _boja;
    int _redniBrojPolja;
    QString _pozadina;
    bool _uZatvoru;

    //QColor _color;
};

#endif // IGRAC_H
