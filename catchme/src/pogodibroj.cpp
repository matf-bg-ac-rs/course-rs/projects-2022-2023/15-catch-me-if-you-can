#include "../include/pogodibroj.h"
#include "ui_pogodibroj.h"
#include <time.h>
#include <stdlib.h>
#include<QDebug>
#include <string>


PogodiBroj::PogodiBroj(QDialog *parent)
    :  QDialog(parent),
    ui(new Ui::PogodiBroj)
{
    ui->setupUi(this);
    this->setWindowTitle("IgricaPogodiBroj");
    ui->lbPoruka->setText("");
    ui->lbBroj->setText(" ");
    srand(time(NULL));

    _tajniBroj = rand()%50 + 1;
    _ispisanBroj=rand()%50 + 1;
    if(_tajniBroj==_ispisanBroj)  {
        if(_tajniBroj==0)
            _tajniBroj+=1;
        else if(_tajniBroj==10)  {
            _tajniBroj-=1;
        }
        else {
            _tajniBroj+=1;
        }
    }

    ui->lbBroj->setText( QString::number(_ispisanBroj));
    ui->lbAkcija->setText("");
    ui->textEdit->setDisabled(true);
}

PogodiBroj::~PogodiBroj()
{
    delete ui;
}

int PogodiBroj::getPomerajAkcija()
{
    return _pomerajAkcija;
}



void PogodiBroj::on_pbManje_clicked()
{
    if(_ispisanBroj>_tajniBroj){
            ui->lbPoruka->setText("Tacan odgovor");
            _pomerajAkcija=1;
            ui->lbAkcija->setText("Broj je: "+ QString::number(_tajniBroj));
        }
        else {
           ui->lbPoruka->setText("Netacan odgovor");
           _pomerajAkcija=-1;
           ui->lbAkcija->setText("Broj je: "+ QString::number(_tajniBroj));
    }
    ui->pbManje->setDisabled(true);
    ui->pbVece->setDisabled(true);

}


void PogodiBroj::on_pbVece_clicked()
{
    if(_ispisanBroj<_tajniBroj){
            ui->lbPoruka->setText("Tacan odgovor");
            _pomerajAkcija=1;
            ui->lbAkcija->setText("Broj je: "+ QString::number(_tajniBroj));
        }
    else {
           ui->lbPoruka->setText("Netacan odgovor");
           _pomerajAkcija=-1;
           ui->lbAkcija->setText("Broj je: " + QString::number(_tajniBroj));

    }
    ui->pbManje->setDisabled(true);
    ui->pbVece->setDisabled(true);

}




