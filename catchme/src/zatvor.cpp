#include "../include/zatvor.h"

Zatvor::Zatvor(int redniBroj, const QList<Igrac*> &igraci)
    : Polje(redniBroj, igraci)
{
}


int Zatvor::akcija()
{
    Igrac* igrac=prikaziIgracaNaPolju();
    igrac->ubaciUZator();
    return 0;
}

Zatvor::~Zatvor() {}
