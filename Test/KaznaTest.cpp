#include <QDebug>
#include "../catchme/include/kazna.h"
#include "../catchme/include/igrac.h"

#include "catch.hpp"


TEST_CASE("klasaaKazna","[class]"){

    SECTION("konstruktor postoji")
    {

        // Arrange
        int redniBrPolja=12;
        QString ime = "nina";
        Igrac* igrac = new Igrac(0,ime,1,Boja::Crna,0);
        QList<Igrac*> igraci={igrac};

        //Act
        Kazna* k = new Kazna(redniBrPolja, igraci);

        // Assert

        REQUIRE_NOTHROW(k);
    }


    SECTION("Akcija vraca vrednost -1")
    {

        // Arrange
        int redniBrPolja=12;
        QString ime = "nina";
        Igrac* igrac = new Igrac(0,ime,1,Boja::Crna,0);
        QList<Igrac*> igraci={igrac};
        Kazna* k = new Kazna(redniBrPolja, igraci);
        int ocekivano = -1;

         // Act
        int rezultat = k->akcija();

        // Assert

         REQUIRE(ocekivano == rezultat);


    }

    SECTION("Desktruktor postoji")
    {
        // Arrange
        QList<Igrac*> igraci=QList<Igrac*>();
        int redniBrPolja=16;
        Kazna* k = new Kazna(redniBrPolja, igraci);

        //Act
        delete k;

        // Assert
        REQUIRE_NOTHROW(k);
    }

}
