#ifndef IMUNITET_H
#define IMUNITET_H

#include "polje.h"

class Imunitet : public Polje
{
public:
    Imunitet(int redniBroj, const QList<Igrac*> &igraci);
    ~Imunitet() override;
    int akcija()  override;
};

#endif // IMUNITET_H
