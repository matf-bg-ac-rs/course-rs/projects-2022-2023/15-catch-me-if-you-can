#ifndef TUNEL_H
#define TUNEL_H

#include "polje.h"

class Tunel : public Polje
{
public:
    Tunel(int redniBroj, const QList<Igrac*> &igraci, int drugiKraj);
    ~Tunel() override;

    int akcija() override;

private:
    int _drugiKraj;

};

#endif // TUNEL_H
