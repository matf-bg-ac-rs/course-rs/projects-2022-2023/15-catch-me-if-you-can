#ifndef PITANJE_H
#define PITANJE_H

#include <QString>
#include<QList>
#include "../include/enummode.h"
class Pitanje
{
public:

    Pitanje(QString textPitanja, QList<QString> odgovori, int tacanOdgovor, Mode mode);
    ~Pitanje() ;

    int getTacanOdgovor() const;
    QList<QString> getOdgovori() const;
    QString getTekstPitanja();

    Mode mode() const;

private:
    QString _tekstPitanja;
    QList<QString> _odgovori;
    int _tacanOdgovor;
    Mode _mode;

};

#endif // PITANJE_H
