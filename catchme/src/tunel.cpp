#include "../include/tunel.h"

Tunel::Tunel(int redniBroj, const QList<Igrac*> &igraci, int drugiKraj)
    : Polje(redniBroj, igraci), _drugiKraj(drugiKraj)
{
}

Tunel::~Tunel()
{

}

int Tunel::akcija()
{
    return _drugiKraj-_redniBroj;
}


