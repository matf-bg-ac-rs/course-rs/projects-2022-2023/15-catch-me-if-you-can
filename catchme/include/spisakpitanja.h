#ifndef SPISAKPITANJA_H
#define SPISAKPITANJA_H

#include <QList>
#include "pitanje.h"
#include "../include/enummode.h"
class SpisakPitanja
{
public:
    SpisakPitanja();
    ~SpisakPitanja();

    Pitanje* izvuciRandomPitanje(Mode mode);
    QList<Pitanje* > filtrirajSpisakPitanja(Mode mdode);
private:
    QList<Pitanje *> _spisakPitanja;

};

#endif // SPISAKPITANJA_H
