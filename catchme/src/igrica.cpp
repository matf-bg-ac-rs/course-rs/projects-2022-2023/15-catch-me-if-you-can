#include "../include/igrica.h"
#include "../include/miniigrazmijica.h"

Igrica::Igrica(int redniBroj, const QList<Igrac * > &igraci)
    :Polje(redniBroj, igraci)
{

}


int Igrica::akcija()  {
    int pomerajAkcija=0;
    Igrac* igrac=prikaziIgracaNaPolju();
    if(_redniBroj == 6){
        _pogodiBroj=new PogodiBroj(nullptr);
        _pogodiBroj->exec();
        pomerajAkcija=_pogodiBroj->getPomerajAkcija();

//        _breakout = new BreakoutGame(nullptr);
//        _breakout->inicijalizuj();
//        _breakout->exec();


//        QTimer timer;
//        QObject::connect(&timer, &QTimer::timeout, _breakout->scena, &QGraphicsScene::advance);

//        // Postavljamo da se advance poziva na svakih 1000/30 ms, sto daje 30fps.
//        timer.start(1000/60);

//        return 1;
    } else {
        _zmijica = new MiniIgraZmijica();
        _zmijica->exec();
        //_zmijica->start();
        pomerajAkcija=_zmijica->getPomerajAkcija();

    }
    if (pomerajAkcija==-1 &&  igrac->getBrojZivota()>2)  {
        return 0;
    }
    return pomerajAkcija;

}

