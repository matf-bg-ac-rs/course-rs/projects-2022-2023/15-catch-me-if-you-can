#ifndef ENUMBOJE_H
#define ENUMBOJE_H

enum class Boja
{
    Roze,
    Crvena,
    Siva,
    Crna,
    Plava,
    Zelena
};

#endif // ENUMBOJE_H
