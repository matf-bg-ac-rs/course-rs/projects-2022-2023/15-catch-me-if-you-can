#ifndef NAGRADA_H
#define NAGRADA_H

#include "polje.h"

class Nagrada : public Polje
{
public:
    Nagrada(int redniBroj, const QList<Igrac*> &igraci);
    ~Nagrada() override;

    int akcija()  override;
};

#endif // NAGRADA_H
