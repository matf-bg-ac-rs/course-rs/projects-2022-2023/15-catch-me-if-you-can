#ifndef BREAKOUTGAME_H
#define BREAKOUTGAME_H

#include <QDialog>
#include <QtGui>
#include <QApplication>
#include <QGraphicsView>
#include <QGraphicsScene>

#include "../include/reket.h"
#include "../include/blok.h"
#include "../include/lopta.h"

namespace Ui {
class BreakoutGame;
}

class BreakoutGame : public QDialog
{
    Q_OBJECT

public:
    explicit BreakoutGame(QWidget *parent = nullptr);
    ~BreakoutGame();

    void inicijalizuj(); 
    void restartuj();

    QGraphicsScene* scena;
    Reket *reket() const;
    QList<Blok *> blokovi() const;

    //int getPomerajAkcija() const;

    //void setPomerajAkcija(int newPomerajAkcija);

    //int _akcija = -1;

    QTimer *timerKraja;

public slots:
    void azurirajAkciju();

private:
    Ui::BreakoutGame *ui;
    Reket* _reket;
    Lopta* _lopta;
    QList<Blok*> _blokovi = {};

   // int _pomerajAkcija = 1;

};

#endif // BREAKOUTGAME_H
