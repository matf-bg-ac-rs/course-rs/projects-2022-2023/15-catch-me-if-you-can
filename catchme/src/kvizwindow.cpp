#include "../include/kvizwindow.h"
#include "ui_kvizwindow.h"
#include <iostream>
kvizWindow::kvizWindow(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::kvizWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Kviz");
    ui->pbZapocni->setDisabled(false);
    ui->lbPitanje->setText("");
    ui->lbPoruka->setText("");
}

kvizWindow::~kvizWindow()
{
    delete ui;
}


void kvizWindow::setPitanje(QString pitanje)  {
    _pitanje=pitanje;

}

void kvizWindow::setTacanOdgovor(int tacanOdg)  {
    _tacanodgovor=tacanOdg;
}

void kvizWindow::setOdgovori(QList<QString> odgovori) {
    _odgovori=odgovori;
}

int kvizWindow::getPomerajAkcija()
{
    return _pomerajAkcija;
}



void kvizWindow::on_pbOdg1_clicked() {
    int korisnikovOdgovor=0;
    ui->pbOdg3->setDisabled(true);
    ui->pbOdg1->setDisabled(true);
    ui->pbOdg2->setDisabled(true);


    if ( _tacanodgovor==korisnikovOdgovor  ) {
        ui->lbPoruka->setText("Tacan odgovor");

        _pomerajAkcija=1;
    }
    else  {
        ui->lbPoruka->setText("Netacan odgovor");
        _pomerajAkcija=-1;

    }
}


void kvizWindow::on_pbOdg2_clicked()
{
    int korisnikovOdgovor=1;
    ui->pbOdg3->setDisabled(true);
    ui->pbOdg1->setDisabled(true);
    ui->pbOdg2->setDisabled(true);


    if ( korisnikovOdgovor==_tacanodgovor ) {

        ui->lbPoruka->setText("Tacan odgovor");
        _pomerajAkcija=1;
    }
    else  {
        ui->lbPoruka->setText("Netacan odgovor");
        _pomerajAkcija=-1;
    }
}






void kvizWindow::on_pbOdg3_clicked()
{
    int korisnikovOdgovor=2;
    ui->pbOdg3->setDisabled(true);
    ui->pbOdg1->setDisabled(true);
    ui->pbOdg2->setDisabled(true);


    if ( korisnikovOdgovor==_tacanodgovor ) {

        ui->lbPoruka->setText("Tacan odgovor");

        _pomerajAkcija=1;
    }
    else  {
        ui->lbPoruka->setText("Netacan odgovor");
        _pomerajAkcija=-1;

    }
}




void kvizWindow::on_pbZapocni_clicked()
{
    ui->lbPitanje->setText(_pitanje);
    ui->pbZapocni->setDisabled(true);
    ui->pbOdg1->setText(_odgovori[0]);
    ui->pbOdg2->setText(_odgovori[1]);
    ui->pbOdg3->setText(_odgovori[2]);
    ui->lbPoruka->setText(" ");
}




