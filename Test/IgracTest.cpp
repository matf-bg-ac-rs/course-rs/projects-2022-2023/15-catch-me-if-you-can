#include <QDebug>
#include "../catchme/include/igrac.h"
#include "catch.hpp"


TEST_CASE("Testiranje konstruktora klase Igrac","[konstruktor]"){
    SECTION("Podrazumevani konstruktor Igrac postoji"){
        //Arrange

        //Act
        Igrac igrac;

        //Assert
        REQUIRE_NOTHROW(igrac);
    }
    SECTION("Konstruktor Igrac postoji"){
        //Arrange
        QString ime="Vasa";
        Boja boja=Boja::Crna;
        //Act
        Igrac* igrac=new Igrac(1,ime,1,boja,0);

        //Assert
        REQUIRE_NOTHROW(igrac);
    }
    SECTION("Pozadina crnog igraca je crni.png"){

        //Arrange
        QString ime="Vasa";
        QString ocekivaniIzlaz="border-image: url(:/resources/images/crni.png);";
        Boja boja=Boja::Crna;
        //Act
        Igrac* igrac=new Igrac(1,ime,1,boja,0);
        QString dobijenIzlaz=igrac->getPozadina();
        //Assert
        REQUIRE(dobijenIzlaz==ocekivaniIzlaz);
    }

    SECTION("Pozadina igraca je prazna ukoliko mu je dodeljena boja za koju trenutno nije dodata pozadina"){

        //Arrange
        QString ime="Vasa";
        QString ocekivaniIzlaz="";
        Boja boja=Boja::Zelena;

        //Act
        Igrac* igrac=new Igrac(1,ime,1,boja,0);
        QString dobijenIzlaz=igrac->getPozadina();

        //Assert
        REQUIRE(dobijenIzlaz==ocekivaniIzlaz);
    }
    SECTION("Igrac inicijalno nije u zatvoru"){

        //Arrange
        QString ime="Vasa";
        bool ocekivaniIzlaz=false;
        Boja boja=Boja::Crna;

        //Act
        Igrac* igrac=new Igrac(1,ime,1,boja,0);
        bool dobijenIzlaz=igrac->uZatvoru();

        //Assert
        REQUIRE(dobijenIzlaz==ocekivaniIzlaz);
    }
}

TEST_CASE("Testiranje destruktora klase Igrac","[destruktor]"){
    SECTION("Destruktor ~Igrac postoji"){
        //Arrange
        Igrac *igrac=nullptr;

        //Act
        igrac=new Igrac();
        delete igrac;

        //Assert
        REQUIRE_NOTHROW(igrac);
    }
}
TEST_CASE("Testiranje ubacivanja i izbacivanja iz zatvora","[funkcija]"){
    SECTION("ubaciUZatvor(); => _uZatvoru==true"){
        QString ime="Vasa";
        bool ocekivaniIzlaz=true;
        Boja boja=Boja::Crna;

        //Act
        Igrac* igrac=new Igrac(1,ime,1,boja,0);
        igrac->ubaciUZator();
        bool dobijeniIzlaz=igrac->uZatvoru();

        REQUIRE(dobijeniIzlaz==ocekivaniIzlaz);
    }
    SECTION("ubaciuZatvor(); izbaciIzZatvora(); => _uZatvoru==false"){
        QString ime="Vasa";
        bool ocekivaniIzlaz=false;
        Boja boja=Boja::Crna;

        //Act
        Igrac* igrac=new Igrac(1,ime,1,boja,0);
        igrac->ubaciUZator();
        igrac->izbaciIzZatvora();
        bool dobijeniIzlaz=igrac->uZatvoru();

        REQUIRE(dobijeniIzlaz==ocekivaniIzlaz);
    }
}
TEST_CASE("Testiranje dodavanja i oduzimanja zivota","[funkcija]"){
    SECTION("Nakon poziva dodajZivot() broj zivota je povecan za 1"){
        //Arrange
        QString ime="Vasa";
        int ulazZivoti=1;
        int ocekivaniIzlaz=2;
        Boja boja=Boja::Crna;
        //Act
        Igrac* igrac=new Igrac(1,ime,ulazZivoti,boja,0);
        igrac->dodajZivot();
        int dobijeniIzlaz=igrac->getBrojZivota();
        //Assert
        REQUIRE(dobijeniIzlaz==ocekivaniIzlaz);
    }
    SECTION("Nakon poziva oduzmiZivot() broj zivota smanjen za 1"){
        //Arrange
        QString ime="Vasa";
        int ulazZivoti=1;
        int ocekivaniIzlaz=0;
        Boja boja=Boja::Crna;
        //Act
        Igrac* igrac=new Igrac(1,ime,ulazZivoti,boja,0);
        igrac->oduzmiZivot();
        int dobijeniIzlaz=igrac->getBrojZivota();
        //Assert
        REQUIRE(dobijeniIzlaz==ocekivaniIzlaz);
    }
}
