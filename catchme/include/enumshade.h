#ifndef ENUMSHADE_H
#define ENUMSHADE_H

enum class Shade
{
    light,
    dark
};

#endif // ENUMSHADE_H
