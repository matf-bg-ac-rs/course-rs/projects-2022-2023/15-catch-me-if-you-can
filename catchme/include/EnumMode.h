#ifndef ENUMMODE_H
#define ENUMMODE_H

enum class Mode
{
    Easy,
    Medium,
    Hard
};

#endif // ENUMMODE_H
