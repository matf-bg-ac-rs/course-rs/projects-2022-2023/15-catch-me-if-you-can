#include <QDebug>
#include "../catchme/include/tunel.h"
#include "../catchme/include/igrac.h"

#include "catch.hpp"


TEST_CASE("Tunel","[class]"){

    SECTION("konstruktor postoji")
           {
        // Arrange
        int redniBrPolja=3;
        int drugiKraj = 15;
        QList<Igrac*> igraci=QList<Igrac*>();
        Tunel* tunel = new Tunel(redniBrPolja, igraci, drugiKraj);

        // Assert

        REQUIRE_NOTHROW(tunel);
    }

    SECTION("Funkcija akcija vraca razliku izmedju polja na koje treba da dodje igrac i na kom se nalazi")
           {

        // Arrange
        int redniBrPolja=10;
        QList<Igrac*> igraci=QList<Igrac*>();
        int drugiKraj = 15;
        Tunel* tunel = new Tunel(redniBrPolja, igraci, drugiKraj);
        int ocekivaniIzlaz = 15 - 10;

        // Assert
        int izlaz = tunel->akcija();

        REQUIRE(izlaz == ocekivaniIzlaz);
    }


    SECTION("Desktruktor postoji")
            {
                // Arrange
                QList<Igrac*> igraci=QList<Igrac*>();
                int drugiKraj = 15;
                Tunel* tunel = new Tunel(10, igraci, drugiKraj);
                delete tunel;

                // Assert
                REQUIRE_NOTHROW(tunel);
            }




}
