# catch me if you can :checkered_flag:
---

## :pencil: Opis igre
---
Drustvena igra koja se sastoji od polja sa pravilima. U svakom koraku baca se kockica, a poenta igre je stici prvi do cilja. Na poljima se moze naci igrica, kviz, nagrada, kazna, imunitet, zatvor i tunel. U slucaju da smo dobro presli igricu (ili kviz) pomeramo se za jedno polje unapred, a inace za jedno polje unazad. Nagradno polje nas pomera za jedno polje napred, a kazneno za jedno polje unazad. Kada stanemo na polje zatvor, preskacemo sledeci krug, a kada stanemo na polje imunitet dobijamo jedan zivot pomocu kog se ne vracamo na startno polje u slucaju da neki drugi igrac stane na nase trenutno polje.Ako igrac stane na pocetno polje tunela, onda automatski prelazi na drugi kraj. U podesavanjima mozemo izabrati tezinu pitanja koja zelimo da se prikazu i rezim osvetljenja pozadine (dark or light).

## :notebook: Okruzenje i koriscene biblioteke:
+ Qt Creator
+ Catch2

## :computer: Programski jezici: 
+ C++ 
+ Qt 5

## :hammer: Instalacija
+ Preuzeti i instalirati *Qt* i *Qt Creator*

## :wrench: Preuzimanje i pokretanje: 
+ 1. U terminalu se pozicionirajte u zeljeni direktoriju
+ 2. Klonirati repozitorijum komandom:  ```  git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/15-catch-me-if-you-can.git      ```



+ 3. Otvoriti okruzenje *Qt Creator* i u njemu otvoriti *catchme.pro* fajl
+ 4. Pritisniti dugme *Run* u donjem levom uglu ekrana




## :video_camera: Demo snimak
+ link : https://youtu.be/4L_wb3L7-kM


## :busts_in_silhouette: Developers
<ul>
    <li><a href="https://gitlab.com/nickla">Nikolina Lazarević 49/2019</a></li>
    <li><a href="https://gitlab.com/vasatodorovic">Vasilije Todorović 25/2019</a></li>
    <li><a href="https://gitlab.com/andjelaa11">Anđela Đurović 26/2019</a></li>
    <li><a href="https://gitlab.com/KatMilosevic">Katarina Milošević 53/2019</a></li>
    <li><a href="https://gitlab.com/ivacitlucanin">Iva Čitlučanin 143/2019</a></li>
</ul>
