#ifndef BLOK_H
#define BLOK_H

#include <QGraphicsRectItem>

class Blok: public QGraphicsRectItem
{
public:
    Blok(QGraphicsItem* parent = nullptr);

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
                  const QStyleOptionGraphicsItem *option,
                  QWidget *widget) override;

};

#endif // BLOK_H
