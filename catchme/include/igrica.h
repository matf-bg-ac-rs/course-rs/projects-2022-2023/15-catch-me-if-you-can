#ifndef IGRICA_H
#define IGRICA_H

#include "polje.h"
#include "../include/pogodibroj.h"
//#include "../include/breakoutgame.h"
#include "../include/miniigrazmijica.h"

class Igrica :public QObject, public Polje
{
public:
    Igrica(int redniBroj, const  QList<Igrac*> &igraci);
    int akcija()   override;

private:
    PogodiBroj* _pogodiBroj;
    MiniIgraZmijica* _zmijica;
};

#endif // IGRICA_H
