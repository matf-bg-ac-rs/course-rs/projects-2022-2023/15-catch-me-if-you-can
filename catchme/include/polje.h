#ifndef POLJE_H
#define POLJE_H

#include"igrac.h"

#include<QList>

class Polje
{
public:
    //Polje() = default;
    Polje()=default;
    Polje(int redniBroj);
    Polje(int redniBroj, const QList<Igrac*> &igraci);

    virtual ~Polje();

    virtual int akcija();
    void staviIgracaNaPolje(Igrac* i);
    void ukloniIgracaSaPolja();
    Igrac* prikaziIgracaNaPolju() const;
    Igrac* prikaziPoslednjeg() const;
    int brojIgraca() const;

    int redniBroj() const;

    Polje dohvatiPolje(int redniBroj) const;

protected:
    int _redniBroj;
    QList<Igrac*> _igraci;
};

#endif // POLJE_H
