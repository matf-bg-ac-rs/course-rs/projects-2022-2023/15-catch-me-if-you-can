#include "../include/lopta.h"

#include "../include/breakoutgame.h"
#include "../include/blok.h"
#include "../include/reket.h"

#include <iostream>

extern BreakoutGame* _breakout;

Lopta::Lopta(QGraphicsRectItem* parent): QObject()
{
    _xBrzina = 0;
    _yBrzina = -5;
    _boja = (Qt::blue);

}

double Lopta::getXkoordCentra()
{
    return pos().x();
}

QList<QGraphicsItem *> Lopta::myCollidingItems(QList<QGraphicsRectItem *> items)
{

    QList<QGraphicsItem *> collidedItems = {};
    for (int i = 0; i < items.count(); i++){
        bool collisionX = sceneBoundingRect().x() + 40 >= items[i]->sceneBoundingRect().x() &&
                items[i]->sceneBoundingRect().x() + items[i]->sceneBoundingRect().width() >= sceneBoundingRect().x();

        bool collisionY = abs(sceneBoundingRect().y()) + 40 >= abs(items[i]->sceneBoundingRect().y()) &&
                abs(items[i]->sceneBoundingRect().y()) + items[i]->sceneBoundingRect().height() >= abs(sceneBoundingRect().y());

        //preklapaju se samo ako je i po x i po y osi
        if(collisionX && collisionY){
            collidedItems.append(items[i]);
        }

    }

    return collidedItems;
}

QList<Blok *> Lopta::myCollidingBlocks(QList<Blok*> items)
{

    QList<Blok*> collidedItems = {};
    for (int i = 0; i < items.count(); i++){
        bool collisionX = sceneBoundingRect().x() + 40 >= items[i]->sceneBoundingRect().x() &&
                items[i]->sceneBoundingRect().x() + items[i]->sceneBoundingRect().width() >= sceneBoundingRect().x();

        bool collisionY = abs(sceneBoundingRect().y()) + 40 >= abs(items[i]->sceneBoundingRect().y()) &&
                abs(items[i]->sceneBoundingRect().y()) + items[i]->sceneBoundingRect().height() >= abs(sceneBoundingRect().y());

        //preklapaju se samo ako je i po x i po y osi
        if(collisionX && collisionY){
            collidedItems.append(items[i]);
        }

    }

    return collidedItems;
}

QRectF Lopta::boundingRect() const
{
    //qreal adjust = -0.5;
    return QRectF(0,0,40,40);
}

void Lopta::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setBrush(_boja);
    painter->drawRect(1,1,40,40);

}

void Lopta::sudarSaIvicomProzora(double sirinaEkrana, double visinaEkrana)
{

    if (mapToScene(rect().topLeft()).x() <= 0){
        _xBrzina = -1 * _xBrzina;
    }

    if (mapToScene(rect().topRight()).x() >= sirinaEkrana){
        _xBrzina = -1 * _xBrzina;
    }

    if (mapToScene(rect().topLeft()).y() <= 0){
        _yBrzina = -1 * _yBrzina;
    }

    //lopta je pala na pod
    if (mapToScene(rect().topRight()).y() >= visinaEkrana){
        //_breakout->hide();
        _breakout->close();
    }
}


void Lopta::sudarSaReketom(QList<QGraphicsItem*> cItems)
{

    for (int i = 0; i < cItems.count(); i++){

        Reket* reket = dynamic_cast<Reket*>(cItems[i]);
        if (reket){

            _yBrzina = -1 * _yBrzina;

           // dodajemo brzinu po x koordinati u zavisnosti od toga gde udari reket
            double xLopta = getXkoordCentra();
            double xReket = reket->getXkoordCentra();

            double dx = xLopta - xReket;
            _xBrzina = (_xBrzina + dx)/15;

       }
   }

}

void Lopta::sudarSaBlokom(QList<QGraphicsItem*> cItems)
{

    for (int i = 0; i < cItems.size(); i++){

        Blok* blok = dynamic_cast<Blok*>(cItems[i]);
        if (blok){
            double xLopta = pos().x();
            double yLopta = pos().y();
            double xBlok = blok->pos().x();
            double yBlok = blok->pos().y();
            double yPad = 15;
            double xPad = 15;

            if(yLopta > yBlok + yPad && _yBrzina < 0){
                _yBrzina = -1 * _yBrzina;
            }

            if(yLopta < yBlok - yPad && _yBrzina > 0){
                _yBrzina = -1 * _yBrzina;
            }

            if(xLopta > xBlok + xPad && _xBrzina < 0){
                _xBrzina = -1 * _xBrzina;
            }

            if(xLopta < xBlok - xPad && _xBrzina > 0){
                _xBrzina = -1 * _xBrzina;
            }

            cItems[i]->setPos(-1000, -1000);
            //cItems[i]->hide();
            //delete blok;

        }
   }

}

void Lopta::advance(int step)
{

    if (!step){
        return;
    }

    double sirinaEkrana = _breakout->scena->width();
    double visinaEkrana = _breakout->scena->height();

    sudarSaIvicomProzora(sirinaEkrana, visinaEkrana);

//    QList<QGraphicsRectItem*> lista = {_breakout->reket()};

//    QList<QGraphicsItem*> cItems = myCollidingItems(lista);

    QList<QGraphicsRectItem*> lista = {_breakout->reket()};
    for(Blok* blok: _breakout->blokovi()){
        lista.append(blok);
    }

    QList<QGraphicsItem*> cItems = myCollidingItems(lista);

    sudarSaReketom(cItems);

//    QList<Blok*> blokItems = myCollidingBlocks(_breakout->blokovi());

    sudarSaBlokom(cItems);

    setPos(mapToParent(_xBrzina,_yBrzina));

    update();
}

double Lopta::xBrzina() const
{
    return _xBrzina;
}

void Lopta::setXBrzina(double newXBrzina)
{
    _xBrzina = newXBrzina;
}

double Lopta::yBrzina() const
{
    return _yBrzina;
}

void Lopta::setYBrzina(double newYBrzina)
{
    _yBrzina = newYBrzina;
}

QColor Lopta::boja() const
{
    return _boja;
}

void Lopta::setBoja(const QColor &newBoja)
{
    _boja = newBoja;
}

