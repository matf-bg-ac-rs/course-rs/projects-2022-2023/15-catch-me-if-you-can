#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include "partija.h"
#include "../include/enummode.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    int brojIgraca() const;

    void setBrojIgraca(int newBrojIgraca);

    QTimer timer;
    //QObject::connect(&timer, &QTimer::timeout, _breakout->scena, &QGraphicsScene::advance);
public slots:
    void odigrajAkciju();

private slots:
    void on_btPravilaIgre_clicked();
    void on_btNazad_clicked();
    void on_btNapustiIgru_clicked();
    void on_btPodesiIgrace_clicked();
    void on_btPodesavanja_clicked();
    void on_pushButton_clicked();
    void on_pbPotvrdi_clicked();
    void on_btZapocniPartiju_clicked();
private:
    Ui::MainWindow *ui;
    Partija* partija;

    int _brojIgraca;
    QList<QString> _imena;
    QList<Boja> _boje;

    bool _zvukUkljucen=false;
    Mode _tezina;

    QList<QLineEdit*> napraviListuLabelaImena();
    QList<QComboBox*> napraviListuOdabiraBoje();
    Boja vratiBoju(int idx);

    void izdvojImenaIgraca();
    void izdvojBrojIgraca();
    void izdvojBojeIgraca();

    void podesiShade();
    void podesiMode();

};
#endif // MAINWINDOW_H
