#include <QDebug>
#include "../catchme/include/polje.h"
#include "../catchme/include/igrac.h"

#include "catch.hpp"


TEST_CASE("Polje","[class]"){


    SECTION("prikaziIgracaNaPolju vraca nullptr ako nema igraca na tom polju")
           {

        // Arrange
         int redniBrPolja=5;

         QList<Igrac*> igraci=QList<Igrac*>();
         Polje *polje=new Polje(redniBrPolja,igraci);


         // Act
        Igrac *rezultat= polje->prikaziIgracaNaPolju();

        // Assert

         REQUIRE(rezultat==nullptr);


           }


    SECTION("prikaziPoslednjeg vraca nullptr ako nema igraca na tom polju")
           {

        // Arrange
         int redniBrPolja=5;

         QList<Igrac*> igraci=QList<Igrac*>();
         Polje *polje=new Polje(redniBrPolja,igraci);


         // Act
        Igrac *rezultat= polje->prikaziPoslednjeg();

        // Assert

         REQUIRE(rezultat==nullptr);

         }

    SECTION("redniBroj vraca redni broj tog polja")
           {

        // Arrange
         int redniBrPolja=5;

         QList<Igrac*> igraci=QList<Igrac*>();
         Polje *polje=new Polje(redniBrPolja,igraci);


         // Act
        int rezultat=polje->redniBroj();


        // Assert

         REQUIRE(rezultat==redniBrPolja);

         }


    SECTION("brojIgraca vraca vrednost za jedan manju od duzine niza igraca koji su stali na to polje nakon poziva funkcije ukloniIgracaSaPolja")
           {

               // Arrange
                int redniBrPolja=5;
                QString ime1="Igrac1";
                Boja boja1=Boja::Crna;
                Igrac* igrac1=new Igrac(1,ime1,boja1);
                igrac1->setRedniBrojPolja(redniBrPolja);

                QString ime2="Igrac2";
                Boja boja2=Boja::Roze;
                Igrac* igrac2=new Igrac(2,ime2,boja2);
                igrac2->setRedniBrojPolja(redniBrPolja);
                QList<Igrac*> igraci;
                igraci.append(igrac1);
                igraci.append(igrac2);

                Polje *polje=new Polje(redniBrPolja,igraci);



                // Act

                polje->ukloniIgracaSaPolja();
               int rezultat=polje->brojIgraca();
               int ocekivaniRezultat=igraci.size()-1;



               // Assert

                REQUIRE(ocekivaniRezultat==rezultat);

           }

    SECTION("staviIgracaNaPolje postavlja igraca na redni broj poja")
           {

               // Arrange
                int redniBrPolja=5;
                QString ime1="Igrac1";
                Boja boja1=Boja::Crna;
                Igrac* igrac1=new Igrac(1,ime1,1,boja1,redniBrPolja);

                Polje *polje=new Polje(redniBrPolja);


                // Act

                polje->staviIgracaNaPolje(igrac1);

                Igrac *ocekivanirezultat=igrac1;
                Igrac *rezultat=polje->prikaziIgracaNaPolju();
               // Assert

                REQUIRE(ocekivanirezultat==rezultat);

           }

    SECTION("ukloniIgracaSaPolja ne radi nista i ne baca exception ako na polju nema igraca")
           {

               // Arrange
                int redniBrPolja=5;

                Polje *polje=new Polje(redniBrPolja);

               // Assert

                REQUIRE_NOTHROW(polje->ukloniIgracaSaPolja());

           }
    SECTION("ukloniIgracaSaPolja uklanja prvog igraca koji je stao na polje, ako stavimo dva igraca na polju ce se posle funkcije nalaziti samo drugi igrac")
           {

               // Arrange
                int redniBrPolja=5;
                QString ime1="Igrac1";
                Boja boja1=Boja::Crna;
                Igrac* igrac1=new Igrac(1,ime1,1,boja1,redniBrPolja);

                QString ime2="Igrac2";
                Boja boja2=Boja::Roze;
                Igrac* igrac2=new Igrac(2,ime2,1,boja2,redniBrPolja);

                Polje *polje=new Polje(redniBrPolja);



                // Act

                polje->staviIgracaNaPolje(igrac1);
                polje->staviIgracaNaPolje(igrac2);
                polje->ukloniIgracaSaPolja();
                Igrac *rezultat=polje->prikaziIgracaNaPolju();

                Igrac *ocekivanirezultat=igrac2;

               // Assert

                REQUIRE(ocekivanirezultat==rezultat);

           }

    SECTION("staviIgracaNaPolje postavlja igraca na redni broj poja")
           {

               // Arrange
                int redniBrPolja=5;
                QString ime1="Igrac1";
                Boja boja1=Boja::Crna;
                Igrac* igrac1=new Igrac(1,ime1,1,boja1,redniBrPolja);

                Polje *polje=new Polje(redniBrPolja);


                // Act

                polje->staviIgracaNaPolje(igrac1);

                Igrac *ocekivanirezultat=igrac1;
                Igrac *rezultat=polje->prikaziIgracaNaPolju();
               // Assert

                REQUIRE(ocekivanirezultat==rezultat);

           }
    SECTION("prikaziIgracaNaPolju i prikaziPoslednjeg vracaju istog igraca ako na polju imamo samo 1 igraca")
           {

               // Arrange
                int redniBrPolja=5;
                QString ime1="Igrac1";
                Boja boja1=Boja::Crna;
                Igrac* igrac1=new Igrac(1,ime1,1,boja1,redniBrPolja);

                Polje *polje=new Polje(redniBrPolja);


                // Act

                polje->staviIgracaNaPolje(igrac1);

                Igrac *rezultat1=polje->prikaziIgracaNaPolju();
                Igrac *rezultat2=polje->prikaziPoslednjeg();
               // Assert

                REQUIRE(rezultat1==rezultat2);

           }


    SECTION("prikaziIgracaNaPolju vraca prvog igraca koji je stao na to polje ")
           {

               // Arrange
                int redniBrPolja=5;
                QString ime1="Igrac1";
                Boja boja1=Boja::Crna;
                Igrac* igrac1=new Igrac(1,ime1,1,boja1,redniBrPolja);

                QString ime2="Igrac2";
                Boja boja2=Boja::Roze;
                Igrac* igrac2=new Igrac(2,ime2,1,boja2,redniBrPolja);

                Polje *polje=new Polje(redniBrPolja);


               // Act

                polje->staviIgracaNaPolje(igrac1);
                polje->staviIgracaNaPolje(igrac2);
                Igrac *ocekivanirezultat=igrac1;
                Igrac *rezultat=polje->prikaziIgracaNaPolju();
               // Assert

                REQUIRE(ocekivanirezultat==rezultat);

           }


    SECTION("Konstruktor postoji")
           {
               // Arrange
               Polje polje;

               // Assert
               REQUIRE_NOTHROW(polje);
           }

    SECTION("Desktruktor postoji")
            {
                // Arrange
                Polje* polje = new Polje();
                delete polje;

                // Assert
                REQUIRE_NOTHROW(polje);
            }




}
