#include "../include/kviz.h"
#include <cstdlib>
#include <iostream>


Kviz::Kviz(int redniBroj, const QList<Igrac *> &igraci, int korisnikovOdgovor)
     : Polje(redniBroj, igraci), _korisnikovOdgovor(korisnikovOdgovor)
{
    _pitanja=new SpisakPitanja();
}

Kviz::~Kviz()
{

}


int Kviz::akcija()
{
    _kvizWindow=new kvizWindow();
    _kvizWindow->setPitanje(_izvucenoPitanje->getTekstPitanja());
    _kvizWindow->setTacanOdgovor(_izvucenoPitanje->getTacanOdgovor());
    _kvizWindow->setOdgovori(_izvucenoPitanje->getOdgovori());

    _kvizWindow->exec();
    Igrac* igrac=prikaziIgracaNaPolju();
    int pomerajAkcija=_kvizWindow->getPomerajAkcija();
    if (pomerajAkcija==-1 && igrac->getBrojZivota() >1)  {
        igrac->oduzmiZivot();
        return 0;
    }
    return pomerajAkcija;
}



void Kviz::izvuci_pitanje(Mode mode)
{
    _izvucenoPitanje = _pitanja->izvuciRandomPitanje(mode);
}






