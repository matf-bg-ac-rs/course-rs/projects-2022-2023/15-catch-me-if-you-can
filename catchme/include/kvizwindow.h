#ifndef KVIZWINDOW_H
#define KVIZWINDOW_H

#include <QDialog>

namespace Ui {
class kvizWindow;
}

class kvizWindow : public QDialog
{
    Q_OBJECT

public:
    explicit kvizWindow( QDialog *parent = nullptr);
    ~kvizWindow();


    void setPitanje(QString pitanje);
    void setTacanOdgovor(int tacanOdg);
    void setOdgovori(QList<QString> odgovori);
    int getPomerajAkcija();

private slots:
    void on_pbOdg1_clicked();

    void on_pbOdg3_clicked();

    void on_pbOdg2_clicked();

    void on_pbZapocni_clicked();


private:
    Ui::kvizWindow *ui;
    QString _pitanje;
    int _tacanodgovor;
    QList<QString> _odgovori;
    int _pomerajAkcija=-1;
};

#endif // KVIZWINDOW_H

