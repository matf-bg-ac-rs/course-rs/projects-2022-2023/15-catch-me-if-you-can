#include "../include/breakoutgame.h"
#include "ui_breakoutgame.h"

#include <iostream>

#include "../include/lopta.h"
#include "../include/blok.h"
#include "../include/reket.h"

BreakoutGame::BreakoutGame(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BreakoutGame)
{
    ui->setupUi(this);
    //scena = new QGraphicsScene(0, 0, 600, 600);

    scena = new QGraphicsScene(0, 0, 400, 600);
    ui->lbPoruka->setVisible(true);
    ui->lbPoruka->setText("Odigrajte zanimljivu mini igru dok cekate na sledece bacanje!");


    ui->graphicsView->setScene(scena);

    timerKraja = new QTimer();
    connect(timerKraja, SIGNAL(timeout()), this, SLOT(azurirajAkciju()));
    timerKraja->start(30000);

    inicijalizuj();

    setStyleSheet(" border-image: url(:/resources/images/background.png) 0 0 0 0 stretch stretch;");
}

BreakoutGame::~BreakoutGame()
{
    delete ui;
}

void BreakoutGame::inicijalizuj()
{
    _lopta = new Lopta();
    //_lopta->setPos(-100,100);
    _lopta->setPos(200,500);
    scena->addItem(_lopta);

    _reket = new Reket();
    //_reket->setPos(-130,140);
    _reket->setPos(150,575);
    scena->addItem(_reket);
    _reket->grabMouse();

    for (int i = 0, n = 6; i < n; ++i){
        for (int j = 0, n = 4; j < n; ++j){
            Blok* blok = new Blok();
            blok->setPos((i+1)*52,j*52);
            _blokovi.append(blok);
            scena->addItem(blok);
        }
    }
}

Reket *BreakoutGame::reket() const
{
    return _reket;
}

QList<Blok *> BreakoutGame::blokovi() const
{
    return _blokovi;
}

void BreakoutGame::restartuj()
{
    ui->lbPoruka->setVisible(false);
    _lopta->setPos(200,500);
    _lopta->setXBrzina(0);
    _lopta->setYBrzina(-5);

    _reket->setPos(150,575);
    int pom = 0;
    for (int i = 0, n = 6; i < n; ++i){

        for (int j = 0, n = 4; j < n; ++j){
            _blokovi[pom]->setPos((i+1)*52,j*52);
            //_blokovi[i+j]->show();
            pom++;
        }
    }

    timerKraja->start(30000);
}

void BreakoutGame::azurirajAkciju()
{
    //ui->lbPoruka->setVisible(true);
    this->hide();
    //_akcija = 1;
}

//int BreakoutGame::getPomerajAkcija() const
//{
//    return _pomerajAkcija;
//}

//void BreakoutGame::setPomerajAkcija(int newPomerajAkcija)
//{
//    _pomerajAkcija = newPomerajAkcija;
//}

