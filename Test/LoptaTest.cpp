#include <QDebug>
#include "../catchme/include/lopta.h"
#include "../catchme/include/breakoutgame.h"
#include "../catchme/include/blok.h"
#include "../catchme/include/reket.h"
#include "../catchme/include/mainwindow.h"
#include "../catchme/include/partija.h"
#include "../catchme/include/enummode.h"
#include "../catchme/include/enumshade.h"
#include "../catchme/include/enumboje.h"
#include "../catchme/include/kazna.h"
#include "../catchme/include/nagrada.h"
#include "../catchme/include/zatvor.h"
#include "../catchme/include/prikazpobednika.h"
#include "../catchme/include/kazna.h"
#include "../catchme/include/igrica.h"
#include "../catchme/include/miniigrazmijica.h"
#include "../catchme/include/igrac.h"


#include "catch.hpp"


TEST_CASE("Lopta", "[konstruktor]"){
    SECTION("Konstruktor klase Lopta ne baca izuzetak kada se pozove"){
        //Arrange
        QGraphicsRectItem* parent = nullptr;

        //Act
        Lopta* lopta = new Lopta(parent);

        //Assert
        REQUIRE_NOTHROW(lopta);
    }

    SECTION("Konstruktor klase Lopta postavlja pocetnu brzinu po x osi na 0, a po y na -3"){
        //Arrange
        int ocekivanaXBrzina = 0;
        int ocekivanaYBrzina = -5;

        //Act
        Lopta* lopta = new Lopta(nullptr);
        int rezultatXBrzine = lopta->xBrzina();
        int rezultatYBrzine = lopta->yBrzina();

        //Assert
        CHECK(ocekivanaXBrzina == rezultatXBrzine);
        REQUIRE(ocekivanaYBrzina == rezultatYBrzine);

    }

    SECTION("Konstruktor klase Lopta, postavlja pocetnu boju na plavu"){
        //Arrange
        QColor ocekivanaBoja = (Qt::blue);;

        //Act
        Lopta* lopta = new Lopta(nullptr);
        QColor rezultatBoje = lopta->boja();

        //Assert
        REQUIRE(ocekivanaBoja == rezultatBoje);

    }

}

TEST_CASE("myCollidingItems", "[funkcija]"){

    SECTION("Ako dodje do kolizije lopte i reketa, reket se dodaje u listu"){

        //Arange
        Lopta* lopta = new Lopta(nullptr);
        lopta->setPos(200,500);
        Reket *reket = new Reket();
        reket->setPos(180,520);
        QList<QGraphicsRectItem*> lista = {reket};
        QList<QGraphicsItem*> ocekivano = {reket};

        //Act
        QList<QGraphicsItem*> rezultat = lopta->myCollidingItems(lista);

        //Assert
        REQUIRE(ocekivano == rezultat);

    }

    SECTION("Ako ne postoji kolizija lopte sa drugim objektima, vraca se prazna lista"){

        //Arange
        Lopta* lopta = new Lopta();
        lopta->setPos(320,500);
        Reket *reket = new Reket();
        reket->setPos(180,570);
        Blok* blok1 = new Blok();
        blok1->setPos(100,50);
        Blok* blok2 = new Blok();
        blok2->setPos(400,230);
        QList<QGraphicsRectItem*> lista = {reket, blok1, blok2};
        QList<QGraphicsItem*> ocekivano = {};

        //Act
        QList<QGraphicsItem*> rezultat = lopta->myCollidingItems(lista);

        //Assert
        REQUIRE(ocekivano == rezultat);

    }

}

TEST_CASE("sudarSaBlokom", "[funkcija]"){

//    SECTION("Ako dodje do kolizije lopte i bloka, blok treba da se obrise"){

//        //Arange
//        Lopta* lopta = new Lopta();
//        lopta->setPos(320,100);
//        Blok* blok = new Blok();
//        blok->setPos(100,50);
//        QList<QGraphicsRectItem*> lista = {blok};
//        QList<QGraphicsItem*> preklopljeniObjekti = lopta->myCollidingItems(lista);
//        QPointF ocekivano = blok->pos();

//        //Act
//        lopta->sudarSaBlokom(preklopljeniObjekti);
//        QPointF rezultat = blok->pos();

//        //Assert
//        REQUIRE_FALSE(ocekivano == rezultat);

//    }

    SECTION("Ako dodje do sudara lopte i donje strane bloka, yBrzina lopte treba da bude pomnozena sa -1"){

        //Arange
        Lopta* lopta = new Lopta();
        lopta->setPos(320,100);
        Blok* blok = new Blok();
        blok->setPos(340,70);
        QList<QGraphicsRectItem*> lista = {blok};
        QList<QGraphicsItem*> preklopljeniObjekti = lopta->myCollidingItems(lista);
        int ocekivano = lopta->yBrzina() * -1;

        //Act
        lopta->sudarSaBlokom(preklopljeniObjekti);
        int rezultat = lopta->yBrzina();

        //Assert
        REQUIRE(ocekivano == rezultat);
    }
}

TEST_CASE("sudarSaReketom", "[funkcija]"){

    SECTION("Ako dodje do sudara lopte i reketa, xBrzina lopte treba da se promeni u zavisnosti od toga gde se sudar desio"){

        //Arange
        Lopta* lopta = new Lopta();
        lopta->setPos(20,15);
        Reket *reket = new Reket();
        reket->setPos(10,15);
        QList<QGraphicsRectItem*> lista = {reket};
        QList<QGraphicsItem*> preklopljeniObjekti = lopta->myCollidingItems(lista);
        int ocekivano = (lopta->xBrzina() + (lopta->getXkoordCentra()-reket->getXkoordCentra()))/15;

        //Act
        lopta->sudarSaBlokom(preklopljeniObjekti);
        int rezultat = lopta->xBrzina();

        //Assert
        REQUIRE(ocekivano == rezultat);
    }
}


TEST_CASE("sudarSaIvicomProzora", "[funkcija]"){

    SECTION("Ako dodje do sudara lopte sa gornjom ivicom prozora, yBrzina lopte treba da bude pomnozena sa -1"){

        //Arange
        double sirina = 400;
        double visina = 600;
        Lopta* lopta = new Lopta();
        lopta->setPos(20,0);
        int ocekivano = lopta->yBrzina() * -1;

        //Act
        lopta->sudarSaIvicomProzora(sirina, visina);
        int rezultat = lopta->yBrzina();

        //Assert
        REQUIRE(ocekivano == rezultat);
    }

    SECTION("Ako dodje do sudara lopte sa levom ivicom prozora, xBrzina lopte treba da bude pomnozena sa -1"){

        //Arange
        double sirina = 400;
        double visina = 600;
        Lopta* lopta = new Lopta();
        lopta->setPos(0,120);
        int ocekivano = lopta->xBrzina() * -1;

        //Act
        lopta->sudarSaIvicomProzora(sirina, visina);
        int rezultat = lopta->xBrzina();

        //Assert
        REQUIRE(ocekivano == rezultat);
    }

    SECTION("Ako dodje do sudara lopte sa desnom ivicom prozora, xBrzina lopte treba da bude pomnozena sa -1"){

        //Arange
        double sirina = 400;
        double visina = 600;
        Lopta* lopta = new Lopta();
        lopta->setPos(370,120);
        int ocekivano = lopta->xBrzina() * -1;

        //Act
        lopta->sudarSaIvicomProzora(sirina, visina);
        int rezultat = lopta->xBrzina();

        //Assert
        REQUIRE(ocekivano == rezultat);
    }

}
