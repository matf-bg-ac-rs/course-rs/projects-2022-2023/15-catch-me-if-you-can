#include "../include/prikazpobednika.h"
#include "ui_prikazpobednika.h"

#include <algorithm>


PrikazPobednika::PrikazPobednika(Igrac* pobednik, QList<Igrac*> igraci, QWidget *parent):
    QWidget(parent),
    ui(new Ui::PrikazPobednika),
    _pobednik(pobednik),
    _igraci(igraci)
{
    ui->setupUi(this);

    inicijalizujListuIgraca();

    QList<Igrac*> ostaliIgraci;
    for (int i=0; i<_igraci.length(); i++) {
        if (_igraci[i] == _pobednik) {
            pobednik = _igraci[i];
        } else {
            ostaliIgraci.append(_igraci[i]);
        }
    }

    std::sort(ostaliIgraci.begin(), ostaliIgraci.end(), [](const Igrac* i1, const Igrac* i2) {
        return (i1->getRedniBrojPolja() > i2->getRedniBrojPolja());
    });

    _labelIgraca[0]->setText(pobednik->getIme());
    QString pozadina = pobednik->getPozadina();
    _labelPijuna[0]->setStyleSheet(pozadina);


    //std::sort(ostaliIgraci.begin(), ostaliIgraci.end(), [](Igrac* i1,Igrac *i2){i1->getRedniBrojPolja() > i2->getRedniBrojPolja();});


    for (int i=0; i<ostaliIgraci.length(); i++) {
        _labelIgraca[i+1]->setText(ostaliIgraci[i]->getIme());
        QString pozadina = ostaliIgraci[i]->getPozadina();
        _labelPijuna[i+1]->setStyleSheet(pozadina);
    }
}

void PrikazPobednika::inicijalizujListuIgraca()
{
    _labelIgraca.append(ui->lb1_pobednik);
    _labelIgraca.append(ui->lb2_pobednik);
    _labelIgraca.append(ui->lb3_pobednik);
    _labelIgraca.append(ui->lb4_pobednik);
    _labelIgraca.append(ui->lb5_pobednik);

    _labelPijuna.append(ui->lbpijun1);
    _labelPijuna.append(ui->lbpijun2);
    _labelPijuna.append(ui->lbpijun3);
    _labelPijuna.append(ui->lbpijun4);
    _labelPijuna.append(ui->lbpijun5);
}

PrikazPobednika::~PrikazPobednika()
{
    delete ui;
}
