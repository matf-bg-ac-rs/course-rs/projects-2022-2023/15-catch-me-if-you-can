#include "../include/igrac.h"

Igrac::Igrac()
{

}

Igrac::Igrac(unsigned int id,  QString &ime,Boja boja) : _id(id),
    _ime(ime),
    _boja(boja)
{
    setPozadina(boja);
    _uZatvoru=false;
    _redniBrojPolja=0;
    _brojZivota=1;
}
Igrac::Igrac(unsigned int id, const QString &ime, int brojZivota, Boja boja, int redniBrojPolja) : _id(id),
    _ime(ime),
    _brojZivota(brojZivota),
    _boja(boja),
    _redniBrojPolja(redniBrojPolja)
{
    setPozadina(boja);
    _uZatvoru=false;
}
Igrac::~Igrac()
{

}

int Igrac::getId() const
{
    return _id;
}


int Igrac::getBrojZivota() const
{
    return _brojZivota;
}

int Igrac::getRedniBrojPolja() const
{
    return _redniBrojPolja;
}

void Igrac::setRedniBrojPolja(int i)
{
    _redniBrojPolja=i;
}

void Igrac::dodajZivot()
{
    _brojZivota+=1;
}

void Igrac::oduzmiZivot()
{
    _brojZivota-=1;
}

void Igrac::ubaciUZator()
{
    _uZatvoru=true;
}

void Igrac::izbaciIzZatvora()
{
    _uZatvoru=false;
}

bool Igrac::uZatvoru() const
{
    return _uZatvoru;
}

QString Igrac::getPozadina() const
{
    return _pozadina;
}

QString Igrac::getIme() const
{
    return _ime;
}

Boja Igrac::getBoja() const
{
    return _boja;
}

void Igrac::setPozadina(const Boja boja)
{
    switch(boja){
        case Boja::Crna:
            _pozadina="border-image: url(:/resources/images/crni.png);";
        break;

        case Boja::Crvena:
            _pozadina="border-image: url(:/resources/images/crveni.png);";
        break;

        case Boja::Plava:
            _pozadina="border-image: url(:/resources/images/plavi.png);";
        break;

        case Boja::Roze:
            _pozadina="border-image: url(:/resources/images/roze.png);";
        break;

        case Boja::Siva:
            _pozadina="border-image: url(:/resources/images/sivi.png);";
        break;
        default:
            _pozadina="";
    }
}

