#ifndef ZATVOR_H
#define ZATVOR_H

#include "polje.h"


class Zatvor : public Polje
{
public:
    Zatvor(int redniBroj, const QList<Igrac*> &igraci);
    ~Zatvor();

    int akcija()  override;
};

#endif // ZATVOR_H
