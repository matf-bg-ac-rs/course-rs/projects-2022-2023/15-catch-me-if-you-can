#ifndef POGODIBROJ_H
#define POGODIBROJ_H

#include <QDialog>
#include "polje.h"


namespace Ui {
class PogodiBroj;
}

class PogodiBroj : public QDialog, public Polje
{
    Q_OBJECT

public:
    explicit PogodiBroj(QDialog *parent);
    ~PogodiBroj();
    int getPomerajAkcija();

private slots:
    void on_pbManje_clicked();

    void on_pbVece_clicked();



private:
    Ui::PogodiBroj *ui;
    int _tajniBroj;
    int _ispisanBroj;
    int _pomerajAkcija=-1;
};

#endif // POGODIBROJ_H
