#ifndef MINIIGRAZMIJICA_H
#define MINIIGRAZMIJICA_H

#include <QDialog>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QBrush>
#include <QGraphicsRectItem>
#include <QKeyEvent>
#include <QList>
#include <QPointF>
#include <QTimer>

namespace Ui {
class MiniIgraZmijica;
}

class MiniIgraZmijica : public QDialog
{
    Q_OBJECT

public:
    explicit MiniIgraZmijica(QWidget *parent = nullptr);
    ~MiniIgraZmijica();

    void start();
    void startGlava();
    void startVocka();
    void startTelo();

    void keyPressEvent(QKeyEvent* event) override;
    void elongate();
    void pomeriTela();

    QGraphicsScene* scene;

    int getPomerajAkcija();

private:
    Ui::MiniIgraZmijica *ui;

    //QGraphicsView* _view;

    QGraphicsRectItem* _glava;
    QGraphicsEllipseItem* _vocka;
    QGraphicsRectItem* _telo;
    QList<QGraphicsRectItem*> _zmijaTela;

    QBrush _glavaBrush;
    QBrush _vockaBrush;
    QBrush _teloBrush;

    QPointF _pozPre;

    int _duzina = 0;
    int _pomerajAkcija = 0;
};

#endif // MINIIGRAZMIJICA_H
