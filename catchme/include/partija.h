#ifndef PARTIJA_H
#define PARTIJA_H

#include <QGraphicsScene>
#include <QWidget>
#include <QtCore>
#include <QtGui>
#include <QLabel>
#include <QLineEdit>

#include "kocka.h"
#include "igrac.h"
#include "polje.h"
#include "enumshade.h"
#include "kazna.h"
#include "nagrada.h"
#include "prikazpobednika.h"
#include "spisakpitanja.h"
#include "pitanje.h"
#include "kviz.h"
#include "tunel.h"
#include "pogodibroj.h"
#include "igrica.h"
#include "zatvor.h"
#include "imunitet.h"
#include "../include/enummode.h"
namespace Ui {
class Partija;
}

class Partija : public QWidget
{
    Q_OBJECT

public:
    explicit Partija(QWidget *parent=nullptr);
    ~Partija();

    QList<Polje *> polja() const;
    int getBrojIgraca() const;
    void setBrojIgraca(int newBrojIgraca);
    void setImena(const QList<QString> &newImena);
    void setBoje(const QList<Boja> &newBoje);
    void podesiProzor();

    Shade shade() const;
    void setShade(Shade shade);
    Mode tezina() const;
    void setMode(Mode tezina);

    bool getPozoviBreakout() const;

private slots:
    void on_btIzadjiIzIgre_partija_clicked();
    void on_btBaciKocku_clicked();

signals:
    void pozoviBreakoutPromenjeno();

private:
    Ui::Partija *ui;
    QGraphicsScene *scene;

    int _naPotezu;
    int _brojIgraca;
    int _brojPolja;

    Kocka _kocka;
    Shade _shade;
    Mode _tezina;

    QList<QLabel*> _labelPolja;
    QList<Igrac*> _igraci;
    QList<Polje*> _polja;
    QList<QString> _imena;
    QList<Boja> _boje;

    void inicijalizujListuPolja();
    void inicijalizacijaLabelaProzora();
    void inicijalizujPoljaSaAkcijama();
    void postaviPozadinu();
    void kockicaSlika(int br);

    int pomeraj(int,int);
    void sledeciIgrac();

    void ukloniIgracaSaStarogPolja(int);
    void jediNaPolju(Polje*);

    QList<QLabel*> vratiListuStartLabela();
    QList<QLineEdit*> vratiListuLineEditaIgraca();
    QLabel* odrediStartnoPolje(Igrac* igrac);

    Igrac* _pobednik;
    bool pobednik();

    bool pozoviBreakout = false;
};

#endif // PARTIJA_H
