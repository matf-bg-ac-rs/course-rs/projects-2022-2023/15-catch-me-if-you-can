#include "../include/reket.h"

Reket::Reket(QGraphicsItem *parent)
{

}

QRectF Reket::boundingRect() const
{
    return QRectF(0,0,100,15);

}

void Reket::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setBrush(Qt::gray);
    painter->drawRect(0,0,100,15);
}

void Reket::mouseMoveEvent(QGraphicsSceneMouseEvent *event){
    // pratimo x koordinatu misa
    double mouseX = mapToScene(event->pos()).x();
    setPos(mouseX,y());
}

double Reket::getXkoordCentra()
{
    return x()+rect().width()/2;
}
