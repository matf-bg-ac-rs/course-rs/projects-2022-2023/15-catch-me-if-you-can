#include <QDebug>
#include "../catchme/include/zatvor.h"
#include "../catchme/include/igrac.h"

#include "catch.hpp"


TEST_CASE("klasaaZatvor","[class]"){

    SECTION("konstruktor postoji")
    {
        // Arrange
        int redniBrPolja=12;
        QList<Igrac*> igraci=QList<Igrac*>();
        Zatvor* zatvor = new Zatvor(redniBrPolja, igraci);

        // Assert

        REQUIRE_NOTHROW(zatvor);
    }

    SECTION("Funkcija akcija vraca prvog igraca iz liste koji ce biti postavljen na polje")
    {

        // Arrange
        int redniBrPolja=12;
        QString ime = "nina";
        Igrac* igrac = new Igrac(0,ime,1,Boja::Crna,0);
        QList<Igrac*> igraci={igrac};
        Zatvor* zatvor = new Zatvor(redniBrPolja, igraci);
        Igrac* ocekivano = igraci[0];


        //Act
        zatvor->akcija();
        Igrac* rezultat = zatvor->prikaziIgracaNaPolju();

        // Assert
        REQUIRE(ocekivano == rezultat);
    }

    SECTION("Funkcija akcija postavlja promenljivu igraca _uZatvoru na true")
    {

        // Arrange
        int redniBrPolja=12;
        QString ime = "nina";
        Igrac* igrac = new Igrac(0,ime,1,Boja::Crna,0);
        QList<Igrac*> igraci={igrac};
        Zatvor* zatvor = new Zatvor(redniBrPolja, igraci);
        bool ocekivano = true;


        //Act
        zatvor->akcija();
        bool rezultat = igrac->uZatvoru();

        // Assert
        REQUIRE(ocekivano == rezultat);
    }

    SECTION("Desktruktor postoji")
    {
        // Arrange
        QList<Igrac*> igraci=QList<Igrac*>();
        int redniBrPolja=12;
        Zatvor* zatvor = new Zatvor(redniBrPolja, igraci);

        //Act
        delete zatvor;

        // Assert
        REQUIRE_NOTHROW(zatvor);
    }




}
