#include "../include/blok.h"

#include <QBrush>
#include <QGraphicsScene>
#include <QPainter>

Blok::Blok(QGraphicsItem *parent)
{

}

QRectF Blok::boundingRect() const
{
    return QRectF(0,0,50,50);
}

void Blok::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setBrush(Qt::green);
    painter->drawRect(0,0,50,50);
    //painter->drawRect(0,0,50,50);
}
