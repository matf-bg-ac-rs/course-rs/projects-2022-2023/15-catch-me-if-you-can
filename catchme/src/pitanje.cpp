#include "../include/pitanje.h"



Pitanje::Pitanje(QString textPitanja, QList<QString> odgovori, int tacanOdgovor, Mode mode)
    :_tekstPitanja(textPitanja), _odgovori(odgovori), _tacanOdgovor(tacanOdgovor), _mode(mode)
{

}

Pitanje::~Pitanje()
{


}

int Pitanje::getTacanOdgovor() const
{
    return _tacanOdgovor;
}



QList<QString> Pitanje::getOdgovori() const
{
    return _odgovori;
}


QString Pitanje::getTekstPitanja()
{
    return _tekstPitanja;
}

Mode Pitanje::mode() const
{
    return _mode;
}
