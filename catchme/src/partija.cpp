#include "../include/partija.h"
#include "ui_partija.h"

#include<QGraphicsScene>
#include<QGraphicsView>
#include<QGraphicsItem>
#include <QTimer>
#include<iostream>
#include<string>

#define BROJ_POLJA 24

Partija::Partija(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Partija)

{
    ui->setupUi(this);
    scene=new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);

    _naPotezu=0;
    _brojPolja=BROJ_POLJA;

    inicijalizujListuPolja();
    inicijalizacijaLabelaProzora();
    inicijalizujPoljaSaAkcijama();
}

Partija::~Partija()
{
    delete ui;
}

void Partija::on_btIzadjiIzIgre_partija_clicked()
{
    qApp->exit();
}

int Partija::getBrojIgraca() const
{
    return _brojIgraca;
}

void Partija::setBrojIgraca(int newBrojIgraca)
{
    _brojIgraca = newBrojIgraca;
}

void Partija::podesiProzor()
{
    postaviPozadinu();
    kockicaSlika(6);

    QList<QLabel*> labelStartPolja=vratiListuStartLabela();
    QList<QLineEdit*> lineEditIgrac=vratiListuLineEditaIgraca();

    for(int i=0;i<_brojIgraca;i++)
    {
        lineEditIgrac[i]->setVisible(true);
        lineEditIgrac[i]->setText(_imena[i]);

        Igrac * igrac=new Igrac(i,_imena[i],_boje[i]);
        _igraci.append(igrac);

        labelStartPolja[i]->setStyleSheet(igrac->getPozadina());
    }

    ui->teIspis->setText("Na potezu je :"+_igraci[0]->getIme());
    ui->laNaPotezu->setStyleSheet(_igraci[0]->getPozadina());
}
void Partija::ukloniIgracaSaStarogPolja(int staroPolje)
{
    _polja[staroPolje]->ukloniIgracaSaPolja();

    if(staroPolje!=0)
        _labelPolja[staroPolje]->setStyleSheet("");
    else{
        QLabel* startnoPolje=odrediStartnoPolje(_igraci[_naPotezu]);
        startnoPolje->setStyleSheet("");
    }
}

void Partija::jediNaPolju(Polje* polje)
{
    if(polje->brojIgraca()==2)
    {
        ui->pojeden->setStyleSheet("border-image:url(:/resources/images/pojeden3.png)");
        //nalazenje igraca koji treba biti pojeden
        Igrac* pojedenIgrac=polje->prikaziIgracaNaPolju();

        //uklanjanje sa polja
        polje->ukloniIgracaSaPolja();
        int idx=polje->redniBroj();
        _labelPolja[idx]->setStyleSheet("");

        //stavljanje na start
        QLabel* startnoPolje=odrediStartnoPolje(pojedenIgrac);
        startnoPolje->setStyleSheet(pojedenIgrac->getPozadina());
        pojedenIgrac->setRedniBrojPolja(0);
        _polja[0]->staviIgracaNaPolje(pojedenIgrac);
    }
}
int Partija::pomeraj(int polje, int broj)
{
    return (polje+broj)%_brojPolja;
}

void Partija::sledeciIgrac()
{
    _naPotezu++;
    _naPotezu=_naPotezu%_brojIgraca;
}
void Partija::on_btBaciKocku_clicked()
{
    ui->pojeden->setStyleSheet("");

    int brojNaKockici=_kocka.baci();
    kockicaSlika(brojNaKockici);

    int staroPolje=_igraci[_naPotezu]->getRedniBrojPolja();
    ukloniIgracaSaStarogPolja(staroPolje);

    int novoPolje=pomeraj(staroPolje,brojNaKockici);

    _igraci[_naPotezu]->setRedniBrojPolja(novoPolje);
    Polje* polje=_polja[novoPolje];
    polje->staviIgracaNaPolje(_igraci[_naPotezu]);

    if (novoPolje ==4 || novoPolje==16 || novoPolje==20)  {
        auto KvizPolje= dynamic_cast<Kviz *> (polje);
        if(_tezina!=Mode::Easy && _tezina!=Mode::Hard && _tezina!=Mode::Medium )  {
            _tezina=Mode::Easy;
        }
        KvizPolje->izvuci_pitanje(_tezina);
    }
    if (novoPolje == 12){
        //pozoviBreakout = true;
        emit pozoviBreakoutPromenjeno();
    }

    jediNaPolju(polje);
    _labelPolja[novoPolje]->setStyleSheet(_igraci[_naPotezu]->getPozadina());

    int a=polje->akcija();
    polje->ukloniIgracaSaPolja(); //pomeranje sa prvog novog polja na drugo novo polje
    _labelPolja[novoPolje]->setStyleSheet("");

    novoPolje=pomeraj(novoPolje,a);
    Polje* polje1=_polja[novoPolje];
    polje1->staviIgracaNaPolje(_igraci[_naPotezu]);
    _igraci[_naPotezu]->setRedniBrojPolja(novoPolje);

    jediNaPolju(polje1);
    _labelPolja[novoPolje]->setStyleSheet(_igraci[_naPotezu]->getPozadina());

    if(pobednik()){
        ui->teIspis->setText("POBEDNIK JE "+_igraci[_naPotezu]->getIme());
        //emit pozoviBreakoutPromenjeno();
        PrikazPobednika* pp = new PrikazPobednika(_pobednik,_igraci);
        pp->show();
        this->hide();
        return;
    }

    sledeciIgrac();
    if(_igraci[_naPotezu]->uZatvoru()){
        _igraci[_naPotezu]->izbaciIzZatvora();
        sledeciIgrac();
    }

    ui->teIspis->setText("Na potezu je "+_igraci[_naPotezu]->getIme());
    ui->laNaPotezu->setStyleSheet(_igraci[_naPotezu]->getPozadina());
}

void Partija::setBoje(const QList<Boja> &newBoje)
{
    _boje = newBoje;
}

void Partija::setImena(const QList<QString> &newImena)
{
    _imena = newImena;
}

QList<Polje *> Partija::polja() const
{
    return _polja;
}

Mode Partija::tezina() const
{
    return _tezina;
}

void Partija::setMode(Mode tezina)
{
    _tezina=tezina;
}

Shade Partija::shade() const
{
    return _shade;
}

void Partija::setShade(Shade newshade)
{
    _shade=newshade;
}

void Partija::inicijalizujListuPolja()
{
    _labelPolja.append(ui->polje_start1);
    _labelPolja.append(ui->polje_1);
    _labelPolja.append(ui->polje_2);
    _labelPolja.append(ui->polje_3);
    _labelPolja.append(ui->polje_4);
    _labelPolja.append(ui->polje_5);
    _labelPolja.append(ui->polje_6);
    _labelPolja.append(ui->polje_7);
    _labelPolja.append(ui->polje_8);
    _labelPolja.append(ui->polje_9);
    _labelPolja.append(ui->polje_10);
    _labelPolja.append(ui->polje_11);
    _labelPolja.append(ui->polje_12);
    _labelPolja.append(ui->polje_13);
    _labelPolja.append(ui->polje_14);
    _labelPolja.append(ui->polje_15);
    _labelPolja.append(ui->polje_16);
    _labelPolja.append(ui->polje_17);
    _labelPolja.append(ui->polje_18);
    _labelPolja.append(ui->polje_19);
    _labelPolja.append(ui->polje_20);
    _labelPolja.append(ui->polje_21);
    _labelPolja.append(ui->polje_22);
    _labelPolja.append(ui->polje_23);
}

void Partija::inicijalizacijaLabelaProzora()
{
    ui->leIgrac1->setEnabled(false);
    ui->leIgrac2->setEnabled(false);
    ui->leIgrac3->setEnabled(false);
    ui->leIgrac4->setEnabled(false);
    ui->leIgrac5->setEnabled(false);

    ui->leIgrac3->setVisible(false);
    ui->leIgrac4->setVisible(false);
    ui->leIgrac5->setVisible(false);
}


void Partija::inicijalizujPoljaSaAkcijama()
{
    QList<Igrac*> igraci=QList<Igrac*>();
    _polja.append(new Polje(0,_igraci));

    for(int i=1;i<_brojPolja;i++){
        if(i==2)
            _polja.append(new Nagrada(i,igraci));
        else if (i==4 || i==16 || i==20){
            _polja.append(new Kviz(i,igraci, -1));
        }
        else if (i==8 || i==22){
            _polja.append(new Kazna(i,igraci));
        }
        else if(i==6 || i==18){
            _polja.append(new Igrica(i, igraci));
        }
        else if (i==10){
            _polja.append(new Tunel(i,igraci,15));
        }
        else if (i==12){
            _polja.append(new Zatvor(i,igraci));
        }
        else if (i==14   )  {
            _polja.append(new Imunitet(i,igraci));
        }
        else{
            _polja.append(new Polje(i,igraci));
        }
    }
}

void Partija::postaviPozadinu()
{
    if(_shade==Shade::light)
        ui->graphicsView->setStyleSheet("border-image:url(:/resources/images/board_light.png)");
    else
        ui->graphicsView->setStyleSheet("border-image:url(:/resources/images/board_dark.png)");
}

QList<QLabel *> Partija::vratiListuStartLabela()
{
    QList<QLabel*> labelStartPolja=QList<QLabel*>();

    labelStartPolja.append(ui->polje_start1);
    labelStartPolja.append(ui->polje_start2);
    labelStartPolja.append(ui->polje_start3);
    labelStartPolja.append(ui->polje_start4);
    labelStartPolja.append(ui->polje_start5);

    return labelStartPolja;
}

QList<QLineEdit *> Partija::vratiListuLineEditaIgraca()
{
    QList<QLineEdit*> lineEditIgrac=QList<QLineEdit*>();

    lineEditIgrac.append(ui->leIgrac1);
    lineEditIgrac.append(ui->leIgrac2);
    lineEditIgrac.append(ui->leIgrac3);
    lineEditIgrac.append(ui->leIgrac4);
    lineEditIgrac.append(ui->leIgrac5);

    return lineEditIgrac;
}

void Partija::kockicaSlika(int br)
{
    if(br==1){
         ui->kockica->setStyleSheet("border-image:url(:/resources/images/dice_1.png)");
    }
    else if(br==2){
         ui->kockica->setStyleSheet("border-image:url(:/resources/images/dice_2.png)");
    }
    else if(br==3){
         ui->kockica->setStyleSheet("border-image:url(:/resources/images/dice_3.png)");
    }
    else if(br==4){
         ui->kockica->setStyleSheet("border-image:url(:/resources/images/dice_4.png)");
    }
    else if(br==5){
         ui->kockica->setStyleSheet("border-image:url(:/resources/images/dice_5.png)");
    }
    else if(br==6){
         ui->kockica->setStyleSheet("border-image:url(:/resources/images/dice_6.png)");
    }
}

bool Partija::pobednik()
{
    int poljeIgraca=_igraci[_naPotezu]->getRedniBrojPolja();
    if(poljeIgraca==0) {
        _pobednik = _igraci[_naPotezu];
        return true;
    }
    return false;
}

bool Partija::getPozoviBreakout() const
{
    return pozoviBreakout;
}

QLabel *Partija::odrediStartnoPolje(Igrac *igrac)
{
    int indeksIgraca=igrac->getId();
    if(indeksIgraca==0)
        return ui->polje_start1;
    if(indeksIgraca==1)
        return ui->polje_start2;
    if(indeksIgraca==2)
        return ui->polje_start3;
    if(indeksIgraca==3)
        return ui->polje_start4;
    return ui->polje_start5;
}

