#include <QDebug>
#include "../catchme/include/kocka.h"

#include "catch.hpp"


TEST_CASE("Kocka","[class]"){


    SECTION("konstruktor postoji")
           {
        Kocka* kocka = new Kocka();

        REQUIRE_NOTHROW(kocka);
    }

    SECTION("Desktruktor postoji")
            {
                // Arrange
                Kocka* kocka = new Kocka();

                delete kocka;

                // Assert
                REQUIRE_NOTHROW(kocka);
            }




}
