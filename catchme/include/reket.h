#ifndef REKET_H
#define REKET_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QBrush>
#include <QGraphicsScene>
#include <QPainter>

class Reket: public QGraphicsRectItem
{
public:
    Reket(QGraphicsItem* parent = nullptr);

    QRectF boundingRect() const override;

    void paint(QPainter *painter,
                  const QStyleOptionGraphicsItem *option,
                  QWidget *widget) override;

    void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;

    double getXkoordCentra();

};

#endif // REKET_H
