#include <QDebug>
#include "../catchme/include/EnumMode.h"
#include "../catchme/include/igrac.h"
#include "../catchme/include/kviz.h"
#include "../catchme/include/spisakpitanja.h"
#include "../catchme/include/pitanje.h"
#include "../catchme/include/kvizwindow.h"
#include "catch.hpp"



TEST_CASE("Pitanje","[class]"){
    SECTION("Konstruktor za lako pitanje postoji"){

        QString textpitanja="Koji je glavni grad Rumunije";
        QList<QString> odg={"Bukurest", "Milano", "Budimpesta"};
        int tacanodg=0;
        Mode mode = Mode::Easy;

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        REQUIRE_NOTHROW(pitanje);
    }

    SECTION("Konstruktor za srednje pitanje postoji"){

        QString textpitanja="Ruteri prosleđuju podatke između uređaja na osnovu: ";
        QList<QString> odg={"IP adresa", "Domena", "MAC adresa"};
        int tacanodg=0;
        Mode mode = Mode::Medium;

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        REQUIRE_NOTHROW(pitanje);
    }

    SECTION("Konstruktor za tesko pitanje postoji"){

        QString textpitanja="Akreditiv je: ";
        QList<QString> odg={"punomoćje ", "padeski oblik", "pozajmica"};
        int tacanodg=0;
        Mode mode = Mode::Hard;

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        REQUIRE_NOTHROW(pitanje);
    }

    SECTION("Funkcija getTacanOdgovor vraca tacan odgovor na to pitanje"){

        QString textpitanja="Koji je glavni grad Rumunije";
        QList<QString> odg={"Bukurest", "Milano", "Budimpesta"};
        int tacanodg=0;
        Mode mode = Mode::Easy;
        int ocekivaniIzlaz = 0;

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        int izlaz = pitanje->getTacanOdgovor();

        REQUIRE(izlaz == ocekivaniIzlaz);
    }

    SECTION("Funkcija getOdgovori vraca tacan ponudjene odgovore na pitanje"){

        QString textpitanja="Koji je glavni grad Rumunije";
        QList<QString> odg={"Bukurest", "Milano", "Budimpesta"};
        int tacanodg=0;
        Mode mode = Mode::Easy;

        QList<QString> ocekivaniIzlaz = {"Bukurest", "Milano", "Budimpesta"};

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        QList<QString> izlaz = pitanje->getOdgovori();

        REQUIRE(izlaz == ocekivaniIzlaz);
    }

    SECTION("Funkcija getTekstPitanja vraca tekst pitanja"){

        QString textpitanja="Koji je glavni grad Rumunije";
        QList<QString> odg={"Bukurest", "Milano", "Budimpesta"};
        int tacanodg=0;
        Mode mode = Mode::Easy;

        QString ocekivaniIzlaz = "Koji je glavni grad Rumunije";

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        QString izlaz = pitanje->getTekstPitanja();

        REQUIRE(izlaz == ocekivaniIzlaz);
    }

    SECTION("Funkcija mode vraca Easy ako je pitanje po tezini Easy"){

        QString textpitanja="Koji je glavni grad Rumunije";
        QList<QString> odg={"Bukurest", "Milano", "Budimpesta"};
        int tacanodg=0;
        Mode mode = Mode::Easy;

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        Mode ocekivaniIzlaz = Mode::Easy;


        Mode izlaz = pitanje->mode();

        REQUIRE(izlaz == ocekivaniIzlaz);
    }

    SECTION("Funkcija mode vraca Medium ako je pitanje po tezini Medium"){

        QString textpitanja="Ruteri prosleđuju podatke između uređaja na osnovu: ";
        QList<QString> odg={"IP adresa", "Domena", "MAC adresa"};
        int tacanodg=0;
        Mode mode = Mode::Medium;

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        Mode ocekivaniIzlaz = Mode::Medium;

        Mode izlaz = pitanje->mode();

        REQUIRE(izlaz == ocekivaniIzlaz);
    }

    SECTION("Funkcija mode vraca Hard ako je pitanje po tezini Hard"){

        QString textpitanja="Akreditiv je: ";
        QList<QString> odg={"punomoćje ", "padeski oblik", "pozajmica"};
        int tacanodg=0;
        Mode mode = Mode::Hard;

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        Mode ocekivaniIzlaz = Mode::Hard;

        Mode izlaz = pitanje->mode();

        REQUIRE(izlaz == ocekivaniIzlaz);
    }

    SECTION("Desktruktor postoji")
            {
                // Arrange
        QString textpitanja="Akreditiv je: ";
        QList<QString> odg={"punomoćje ", "padeski oblik", "pozajmica"};
        int tacanodg=0;
        Mode mode = Mode::Hard;

        Pitanje* pitanje =new Pitanje(textpitanja, odg, tacanodg, mode);

        delete pitanje;

                // Assert
                REQUIRE_NOTHROW(pitanje);
            }


}


